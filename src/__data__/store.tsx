import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';
import userReducer from './feature/user/user.slice';
import tabReducer from './feature/reservation/reservation.slice';
import { restaurantApi } from './api';
import { basketReducer } from './basket/basket.slice';

export const setupStore = (defaultState?) => {
    const store = configureStore({
        preloadedState: defaultState,
        reducer: {
            user: userReducer,
            reservation: tabReducer,
            [restaurantApi.reducerPath]: restaurantApi.reducer,
            basket: basketReducer
        },
        middleware: (getDefaultMiddleware) =>
            getDefaultMiddleware().concat(restaurantApi.middleware)
    });

    setupListeners(store.dispatch);
    return store;
};

export const store = setupStore();

export type RootState = ReturnType<typeof store.getState>;
