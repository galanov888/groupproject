// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
require('dotenv').config();
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const router = require('express').Router();
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const authRouter = require('./routes/auth');
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const dishRouter = require('./routes/dish');
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const reservationRouter = require('./routes/reservation');
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const errorHandlingMidleware = require('./midlewares/errorHandling.midleware');
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const userMidleware = require('./midlewares/user.midleware');
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const orderRouter = require('./routes/order');

// const app = express();
router.use('/dish', dishRouter);
router.use(reservationRouter);
router.use(orderRouter);
router.use('/auth', authRouter);

router.use(userMidleware);
router.use(router);
router.use(errorHandlingMidleware);

// eslint-disable-next-line no-undef
module.exports = router;
