const waitMidleware = (req, res, next) => {
    setTimeout(next, 500);
};

// eslint-disable-next-line no-undef
module.exports = waitMidleware;
