import React from 'react';
import ReactDOM from 'react-dom';
import {
    ToasterContainer,
    ToasterMessage,
    ToasterClose,
    ToasterOverlay
} from './toaster.style';

type ToasterProps = {
    isOpen: boolean;
    message?: string;
    handleClose?: () => void;
};

const Toaster: React.FC<ToasterProps> = ({ isOpen, message, handleClose }) => {
    if (!isOpen) return null;
    return ReactDOM.createPortal(
        <ToasterOverlay>
            <ToasterContainer>
                <ToasterMessage data-testid={'toaster-message'}>
                    {message}
                </ToasterMessage>
                <ToasterClose
                    onClick={handleClose}
                    data-testid={'toaster-close'}
                >
                    X
                </ToasterClose>
            </ToasterContainer>
        </ToasterOverlay>,
        document.body
    );
};
export default Toaster;
