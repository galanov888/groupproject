import React from 'react';
import { DishCategories, DishType } from '../../../../data/dishes';
import MenuItem from '../menu-item/menu-item';
import {
    StyledMenuCategory,
    StyledCategory,
    StyledStaples,
    StyledItems
} from './menu-category.style';

const MenuCategory: React.FC<{
    category: DishCategories;
    dishs: DishType[];
}> = ({ category, dishs }) => {
    return (
        <StyledMenuCategory aria-label={'category'}>
            <StyledCategory>{`"${category}":`}</StyledCategory>
            <StyledStaples>{`{`}</StyledStaples>
            <StyledItems>
                {dishs.map((item) => {
                    return <MenuItem key={item.id} dish={item} />;
                })}
            </StyledItems>

            <StyledStaples>{'}'}</StyledStaples>
        </StyledMenuCategory>
    );
};

export default MenuCategory;
