export const getTitlePrice = (number: number) => {
    return `${number} ₽`;
};

export const get16BytePrice = (number: number) => `0x${number.toString(16)} ₽`;
