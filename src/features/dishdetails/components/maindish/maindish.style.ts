import styled from '@emotion/styled';
import Counter from '../../../counter/counter';

export const StyledDishContainer = styled.div`
    flex-wrap: wrap;
    display: flex;
    padding-bottom: ${(props) => props.theme.spacing.xxl};
    padding-top: ${(props) => props.theme.spacing.xxl};
`;

export const StyledDishDetails = styled.div`
    padding-left: ${(props) => props.theme.spacing.m};
`;

export const StyledDishTitle = styled.div`
    font-family: ${(props) => props.theme.font.main};
    align-content: center;
    color: ${(props) => props.theme.color.main};
    font-size: ${(props) => props.theme.fontSize.m};
    padding-bottom: ${(props) => props.theme.spacing.xs};
`;

export const StyledField = styled.div`
    font-family: ${(props) => props.theme.font.main};
    color: ${(props) => props.theme.color.decor3};
    font-size: ${(props) => props.theme.fontSize.s};
    padding-left: ${(props) => props.theme.spacing.l};
    padding-right: ${(props) => props.theme.spacing.xs};
`;

export const StyledValue = styled.div`
    font-family: ${(props) => props.theme.font.main};
    color: ${(props) => props.theme.color.main};
    font-size: ${(props) => props.theme.fontSize.s};
    width: 600px;
`;

export const StyledRow = styled.div`
    flex-wrap: wrap;
    display: flex;
    padding-bottom: 5px;
`;
export const Decor = styled.span`
    color: ${(props) => props.theme.color.decor1};
`;

export const StyledCounter = styled(Counter)`
    padding-top: 50px;
    color: greenyellow;
`;
export const StyledDishPrice = styled.div`
    color: ${(props) => props.theme.color.decor2};
`;
