import styled from '@emotion/styled';

export const StyledAlsolike = styled.div`
    font-family: ${(props) => props.theme.font.main};
    color: ${(props) => props.theme.color.main};
    font-size: ${(props) => props.theme.fontSize.m};
    padding-bottom: ${(props) => props.theme.spacing.m};
`;
export const StyledRecommendations = styled.div`
    flex-wrap: wrap;
    display: flex;
    padding-bottom: ${(props) => props.theme.spacing.l};
    border: 2px solid ${(props) => props.theme.color.debug};
`;
