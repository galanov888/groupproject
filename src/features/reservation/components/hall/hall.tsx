import React from 'react';
import Tab from '../tab/tab';

type HallProps = {
    tabs: string[];
};
const Hall: React.FC<HallProps> = ({ tabs }) => {
    return (
        <table>
            <tbody>
                <tr key={1}>
                    {tabs.slice(0, 3).map((item, index) => (
                        <td key={index}>
                            <Tab tabId={item} />
                        </td>
                    ))}
                </tr>
                <tr key={2}>
                    {tabs.slice(3, 6).map((item, index) => (
                        <td key={index}>
                            <Tab tabId={item} />
                        </td>
                    ))}
                </tr>
            </tbody>
        </table>
    );
};

export default Hall;
