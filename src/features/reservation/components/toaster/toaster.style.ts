import styled from '@emotion/styled';

export const ToasterOverlay = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    backdrop-filter: blur(1px);
`;
export const ToasterContainer = styled.div`
    opacity: 0.9;
    position: fixed;
    top: 50%;
    left: 50%;
    width: 300px;
    height: 100px;
    transform: translate(-50%, -50%);
    background: ${(props) => props.theme.color.background};
    padding: 10px;
    border: 2px solid ${(props) => props.theme.color.decor4};
    border-radius: 5px;
    box-shadow: 0 0 7px ${(props) => props.theme.color.main};
`;

export const ToasterMessage = styled.div`
    display: flex;
    justify-content: center;
    font-family: ${(props) => props.theme.font.main};
    font-size: ${(props) => props.theme.fontSize.xs};
    color: ${(props) => props.theme.color.decor3};
`;

export const ToasterClose = styled.button`
    font-size: ${(props) => props.theme.fontSize.xs};
    cursor: pointer;
    position: absolute;
    left: calc(100% - 25px);
    bottom: calc(100% - 25px);
    background: ${(props) => props.theme.color.background};
    color: ${(props) => props.theme.color.main};
    width: 25px;
    height: 25px;
    border: none;
    z-index: 1;
`;
