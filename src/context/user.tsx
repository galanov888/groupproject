import React, { useState } from 'react';
import { User } from '../data/user';

export const UserContext = React.createContext<User>(null);

export const UserContextProvider: React.FC = (props) => {
    const [user, setUser] = useState(null);

    return (
        <UserContext.Provider
            value={{
                nick: ''
            }}
        ></UserContext.Provider>
    );
};
