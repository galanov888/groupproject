const pkg = require('./package');
const ESLintPlugin = require('eslint-webpack-plugin');

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version
                }/`,
        },
        plugins: [new ESLintPlugin({ extensions: ['js', 'ts', 'tsx'] })],
    },
    navigations: {
        'restaurant.main': '/restaurant',
        'restaurant.home': '/',
        'restaurant.menu': '/menu',
        'restaurant.contacts': '/contacts',
        'restaurant.dish': '/dish/:dishId',
        'restaurant.feedback': '/feedback',
        'restaurant.reservation': '/reservation',
        'restaurant.cart': '/cart',
        'restaurant.login': '/login',
        'restaurant.registration': '/registration',
        'restaurant.account': '/account'
    },
    features: {
        restaurant: {
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        key: 'value',
        // for server
        // apiUrl: '/multystub/stc-22-24',
        // for local server 
        // apiUrl: 'http://localhost:8043//stc-22-24',
        'restaurant.apiUrl': '/api'
    },
};
