import React from 'react';
import { DishType } from '../../../../data/dish/dishtypes';
import { StyledDishImage } from '../common/dish.style';
import {
    StyledDishContainer,
    StyledDishDetails,
    StyledDishTitle,
    StyledValue,
    StyledField,
    StyledRow,
    Decor
} from './maindish.style';

import Counter from '../../../counter/counter';

type MainDishProps = {
    dish: DishType;
};

const MainDish: React.FC<MainDishProps> = ({ dish }) => {
    const imgPath = `${__webpack_public_path__}/remote-assets/dish/${dish.image}`;
    return (
        <StyledDishContainer data-testid={`maindish-${dish.id}`}>
            <StyledDishImage small={false} src={imgPath} alt={dish.title} />
            <StyledDishDetails>
                <StyledDishTitle>{`${dish.title} {`}</StyledDishTitle>
                <StyledRow>
                    <StyledField>category: </StyledField>
                    <StyledValue>
                        {`"${dish.category}"`}
                        <Decor>{`,`}</Decor>
                    </StyledValue>
                </StyledRow>
                <StyledRow>
                    <StyledField>description: </StyledField>
                    <StyledValue>
                        {`"${dish.description}"`}
                        <Decor>{`,`}</Decor>
                    </StyledValue>
                </StyledRow>
                <StyledRow>
                    <StyledField>price: </StyledField>
                    <StyledValue>
                        0x{dish.price.toString(16)}₽<Decor>{`,`}</Decor>
                    </StyledValue>
                </StyledRow>
                <StyledDishTitle>{`}`}</StyledDishTitle>
                <Counter dish={dish} />
            </StyledDishDetails>
        </StyledDishContainer>
    );
};

export default MainDish;
