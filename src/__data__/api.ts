import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { getConfigValue } from '@ijl/cli';
import { TabType } from '../data/reservation/tabtype';
import { DishType } from '../data/dish/dishtypes';
import { OrderResponse } from '../data/order/order.response';
import { OrderRequest } from '../data/order/order.request';

export interface Response<T> {
    success: boolean;
    error: string;
    data: T;
}

const apiUrl = getConfigValue('restaurant.apiUrl');
export const restaurantApi = createApi({
    reducerPath: 'api',
    baseQuery: fetchBaseQuery({ baseUrl: apiUrl }),
    endpoints: (builder) => ({
        getTabs: builder.query<TabType[], string>({
            query: (name) => `reservation/`
        }),

        postTabs: builder.mutation<undefined, TabType[]>({
            query: (data) => ({
                url: `reservation/post`,
                method: 'POST',
                body: data
            })
        }),

        getDish: builder.query<Response<DishType>, string>({
            query: (dishId) => `dish/${dishId}/`
        }),

        getAlsoLike: builder.query<Response<DishType[]>, string>({
            query: (dishId) => `dish/alsoLike/${dishId}/`
        }),
        addOrder: builder.mutation<OrderResponse, OrderRequest>({
            query: (data) => ({
                url: 'order',
                method: 'POST',
                body: data
            })
        })
    })
});

export const {
    useGetTabsQuery,
    usePostTabsMutation,
    useGetDishQuery,
    useGetAlsoLikeQuery,
    useAddOrderMutation
} = restaurantApi;
