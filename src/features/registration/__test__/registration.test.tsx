import React from 'react';
import './matchMedia';
import 'whatwg-fetch';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { describe, it, beforeAll, afterEach, afterAll } from '@jest/globals';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom/extend-expect';
import { act } from 'react-dom/test-utils';

import Registration from '../registration';

import { ThemeContextProvider } from '../../../context/themeContext';
import showNotification from '../notification';

const server = setupServer(
    rest.post('/auth/register', async (req, res, ctx) => {
        const { email, password } = await req.json();
        if (email !== 'email@test.ru' || password !== '12345') {
            return res(
                ctx.json({
                    success: false,
                    error: 'Invalid email or password'
                })
            );
        }
        return res(
            ctx.json({
                success: true,
                data: {
                    email: 'email',
                    token: 'token'
                }
            })
        );
    })
);
const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
    ...(jest.requireActual('react-router-dom') as any),
    useNavigate: () => mockedUsedNavigate
}));

jest.mock('../notification', () => ({
    __esModule: true,
    default: jest.fn()
}));

beforeAll(() => {
    server.listen();
});
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('Registration', () => {
    it('Snapshot', () => {
        const { container } = render(
            <ThemeContextProvider>
                <BrowserRouter>
                    <Registration />
                </BrowserRouter>
            </ThemeContextProvider>
        );
    });

    it('Registration test ', async () => {
        const { container } = render(
            <ThemeContextProvider>
                <BrowserRouter>
                    <Registration />
                </BrowserRouter>
            </ThemeContextProvider>
        );

        act(() => {
            fireEvent.change(
                screen.getByRole('textbox', {
                    name: /email/i
                }),
                {
                    target: { value: 'email@test.ru' }
                }
            );
        });
        expect(screen.getByDisplayValue('email@test.ru')).toHaveValue(
            'email@test.ru'
        );
        act(() => {
            fireEvent.change(screen.getByLabelText(/password/i), {
                target: { value: '1234' }
            });
        });
        expect(screen.getByLabelText(/password/i)).toHaveValue('1234');

        act(() => {
            fireEvent.click(screen.getByRole('button', { name: /submit/i }));
        });

        await waitFor(() => {
            expect(showNotification).toHaveBeenCalledTimes(1);
        });

        act(() => {
            fireEvent.change(screen.getByLabelText(/password/i), {
                target: { value: '12345' }
            });
        });
        expect(screen.getByLabelText(/password/i)).toHaveValue('12345');
        act(() => {
            fireEvent.click(screen.getByRole('button', { name: /submit/i }));
        });
        await waitFor(() => {
            expect(mockedUsedNavigate.mock.calls.length).toBe(1);
        });
        expect(
            screen.getByRole('textbox', {
                name: /email/i
            })
        ).toHaveValue('');
        expect(screen.getByLabelText(/password/i)).toHaveValue('');
    });
});
