import styled from '@emotion/styled';

export const StyledAboutDiv = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    height: 400px;
    background-color: #252525;
`;

export const StyledAboutImg = styled.img`
    height: 80%;
    background-color: #252525;
    margin: 10px;
`;

export const StyledAboutText = styled.p`
    height: 20px;
    margin: 10px;
    text-align: justify;
    text-indent: 30px;
    font-family: ${(props) => props.theme.font.main};
`;
