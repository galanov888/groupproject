import { DishType, DishCategories } from './dishtypes';

class DishDataService {
    readonly data: DishType[];
    constructor(data: DishType[]) {
        this.data = data;
    }

    getDishById = (dishId: string): DishType => {
        return this.data.find((el) => el.id === dishId);
    };

    getCategoryArrayByName = (category: DishCategories): DishType[] => {
        return this.data.filter((el) => el.category === category);
    };

    getSiblings = (dishId: string): DishType[] => {
        const dish: DishType = this.getDishById(dishId);
        const category: DishType[] = this.getCategoryArrayByName(dish.category);
        return category.filter((el) => el.id !== dishId);
    };

    groupBy<T, K extends keyof T>(array: T[], key: K) {
        const map = new Map<T[K], T[]>();
        array.forEach((item) => {
            const itemKey = item[key];
            if (!map.has(itemKey)) {
                map.set(
                    itemKey,
                    array.filter((i) => i[key] === item[key])
                );
            }
        });
        return map;
    }
}
export default DishDataService;

//Usage
/*   
import DataProvider from '../../data/dish/dishdataservice';
import data from '../../data/dish/dishdata';
import { DishCategories } from '../../data/dish/dishtypes';

const DishPage = () => {
    const d = new DataProvider(data);
    const id = 's2';
    const dish = d.getDishById(id);
    const siblings = d.getSiblings(id);

    for (const cat in DishCategories) {
        console.log(
            'console.log(d.getCategoryArrayByName)=',
            d.getCategoryArrayByName(DishCategories[cat])
        );
    }
    return (
        <>
        </>
    );
};
 */
