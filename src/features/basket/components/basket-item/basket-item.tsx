import React from 'react';
import { DishType } from '../../../../data/dish/dishtypes';
import Counter from '../../../counter/counter';
import {
    StyledText,
    StyledDivInLine,
    StyledBasketItemBox,
    StyledTextPrice,
    StyledCounterBasket
} from './basket-item.style';
import { getTitlePrice, get16BytePrice } from '../../utils';

type ItemData = {
    item: DishType;
};

const BasketItem: React.FC<ItemData> = (props) => {
    const { item } = props;
    return (
        <>
            <StyledBasketItemBox>
                <StyledDivInLine>
                    <StyledText>{item.title}</StyledText>
                    <StyledTextPrice title={getTitlePrice(item.price)}>
                        {get16BytePrice(item.price)}
                    </StyledTextPrice>
                    <StyledCounterBasket>
                        <Counter dish={item} />
                    </StyledCounterBasket>
                </StyledDivInLine>
            </StyledBasketItemBox>
        </>
    );
};

export default BasketItem;
