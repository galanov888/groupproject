import styled from '@emotion/styled';

export const PopupBox = styled.div`
    position: fixed;
    background: #00000050;
    width: 100%;
    height: 100vh;
    top: 0;
    left: 0;
`;

export const StyledCloseIcon = styled.button`
    content: 'x';
    cursor: pointer;
    position: absolute;
    left: calc(100% - 12px);
    bottom: calc(100% - 12px);
    background: ${(props) => props.theme.color.background};
    width: 25px;
    height: 25px;
    border-radius: 50%;
    line-height: 20px;
    text-align: center;
    border: 1px solid #999;
    font-size: 20px;
    z-index: 1;
`;

export const Box = styled.div`
    position: relative;
    width: 20%;
    margin: 0 auto;
    height: auto;
    max-height: 70vh;
    margin-top: calc(120vh - 85vh - 20px);
    background: ${(props) => props.theme.color.background};
    border-radius: 4px;
    padding: 20px;
    border: 1px solid #999;
`;

export const StyledInput = styled.input`
    font-family: ${(props) => props.theme.font.main};
    color: ${(props) => props.theme.color.decor1};
    background: ${(props) => props.theme.color.background};
    display: block;
    max-width: 250px;
    width: 100%;
`;

export const StyledHeader = styled.div`
    font-family: ${(props) => props.theme.font.main};
    font-size: ${(props) => props.theme.fontSize.m};
    color: ${(props) => props.theme.color.decor1};
`;

export const StyledLabel = styled.label`
    font-family: ${(props) => props.theme.font.main};
    color: ${(props) => props.theme.color.decor1};
    background: ${(props) => props.theme.color.background};
    display: block;
`;

export const StyledError = styled.div`
    font-family: ${(props) => props.theme.font.main};
    color: red;
    background: ${(props) => props.theme.color.background};
`;
