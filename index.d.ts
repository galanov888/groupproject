import { Day } from './src/theme';

declare module '@emotion/react' {
    type RestaurantTheme = typeof Day;

    export interface Theme extends RestaurantTheme {}
}

declare module '*.jpeg' {
    const path: string;
    export default path;
}
