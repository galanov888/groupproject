import React from 'react';
import RecommendedItem from '../recommendeddish/recommended';
import { DishType } from '../../../../data/dish/dishtypes';
import { StyledAlsolike, StyledRecommendations } from './recommendations.style';

type DishOfferProps = {
    category: DishType[];
};

const Recommendations: React.FC<DishOfferProps> = ({ category }) => {
    return (
        <div>
            <StyledAlsolike>
                {category[0].category} you may also like:
            </StyledAlsolike>
            <StyledRecommendations>
                {category.map((item) => (
                    <div key={item.id}>
                        <RecommendedItem dish={item} />
                    </div>
                ))}
            </StyledRecommendations>
        </div>
    );
};

export default Recommendations;
