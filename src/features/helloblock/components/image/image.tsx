import styled from '@emotion/styled';

export const StyledImg = styled.img`
    width: 100%;
    height: 100%;
    filter: brightness(40%);
`;
