import React from 'react';
import { describe, it, expect } from '@jest/globals';
import { render, screen } from '@testing-library/react';
import Footer from '../footer';
import '@testing-library/jest-dom';
import { ThemeContextProvider } from '../../../context/themeContext';

describe('Component Footer', () => {
    it('alive', () => {
        const { container } = render(
            <ThemeContextProvider>
                <Footer />
            </ThemeContextProvider>
        );
        expect(container).toMatchSnapshot();
        expect(screen.getByRole('contentinfo').textContent).toBe(
            'Team1: Nikita Galanov, Sergey Kashenkov, Natalia Chesnokovskaya'
        );
    });
});
