import styled from '@emotion/styled';

export const StyledTitle = styled.div`
    font-family: ${(props) => props.theme.font.main};
    font-size: ${(props) => props.theme.fontSize.l};
    color: ${(props) => props.theme.color.main};
    padding-bottom: 20px;
    display: flex;
    justify-content: center;
`;

export const StyledDate = styled.div`
    font-family: ${(props) => props.theme.font.main};
    font-size: ${(props) => props.theme.fontSize.s};
    color: ${(props) => props.theme.color.decor3};
    display: flex;
    justify-content: center;
    padding-bottom: 20px;
`;
