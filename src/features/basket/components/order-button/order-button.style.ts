import styled from '@emotion/styled';

export const StyledOrderButton = styled.button`
    height: ${(props) => props.theme.spacing.l};
    font-size: ${(props) => props.theme.fontSize.m};
    font-family: ${(props) => props.theme.font.title};
    color: ${(props) => props.theme.color.header};
    border: 1px solid ${(props) => props.theme.color.header};
    border-radius: 3px;
    text-align: right;
    margin: 20px;
    font-family: ${(props) => props.theme.font.title};
`;
