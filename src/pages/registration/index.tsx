import React from 'react';
import RegistrationForm from '../../features/registration/registration';

const Registration: React.FC = () => {
    return (
        <div>
            <RegistrationForm />
        </div>
    );
};

export default Registration;
