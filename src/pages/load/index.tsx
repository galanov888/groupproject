import React, { useContext } from 'react';
import { MyThemeContext } from '../../context/themeContext';
import { Player } from '@lottiefiles/react-lottie-player';
import {
    LoadingAnimationDark,
    LoadingAnimationLight
} from '../../../remote-assets/animations/loading';
import { Night } from '../../theme';
import styled from '@emotion/styled';

const StyledPlayer = styled(Player)`
    width: 300px;
    height: 300px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%);
`;

const LoadingPage = () => {
    const { theme } = useContext(MyThemeContext);
    const anim = theme === Night ? LoadingAnimationDark : LoadingAnimationLight;
    return <StyledPlayer autoplay loop src={anim}></StyledPlayer>;
};
export default LoadingPage;
