import * as React from 'react';
import {
    describe,
    it,
    expect,
    beforeAll,
    afterEach,
    afterAll
} from '@jest/globals';
import { render, waitFor, screen, fireEvent } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import '@testing-library/jest-dom';
import data from './reservation.test.json';
import { Provider } from 'react-redux';
import { setupStore } from '../../../__data__/store';
import 'whatwg-fetch';
import { ThemeContextProvider } from '../../../context/themeContext';
import { BrowserRouter } from 'react-router-dom';
import ReservationPage from '../../../pages/reservation';

const server = setupServer(
    rest.get('/reservation/', (req, res, ctx) => {
        return res(ctx.json(data));
    }),

    rest.post('/reservation/post', (req, res, ctx) => {
        return res(ctx.status(500));
    })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('Reservation Component Tests 2', () => {
    const isReserved = (id: string) =>
        screen.getByTestId(id).getAttribute('data-reserved');
    const isSelected = (id: string) =>
        screen.getByTestId(id).getAttribute('data-selected');
    const calendarInput =
        'div[class="react-datepicker__input-container"] input';
    const testDate = '2024-3-17';
    const setCalendarDate = async (container: HTMLElement, date: string) => {
        await waitFor(() => container.querySelector(calendarInput));
        fireEvent.change(container.querySelector(calendarInput), {
            target: { value: date }
        });
    };
    const loaded = async () => {
        await waitFor(() => screen.getByTestId('t1'));
    };

    it('Test1: Reservation reverts data if server returns isErros', async () => {
        const store = setupStore();
        const { container } = render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <BrowserRouter>
                        <ReservationPage />
                    </BrowserRouter>
                </Provider>
            </ThemeContextProvider>
        );
        const id1 = 't4';
        const rec1 = 't400';

        await loaded();
        await setCalendarDate(container, testDate);

        await waitFor(() => screen.getByTestId(id1));

        expect(isReserved(id1) && isSelected(id1)).toBe('false');
        fireEvent.click(screen.getByTestId(rec1));
        await waitFor(() => screen.getByTestId(id1));
        expect(isSelected(id1)).toBe('true');

        fireEvent.click(screen.getByTestId('reservation'));
        await waitFor(() => screen.getByTestId('modal-message'));
        fireEvent.click(screen.getByTestId('modal-ok'));

        await waitFor(() => screen.getByTestId('toaster-message'));
        const text = screen.getByTestId('toaster-message').textContent;
        expect(text).toBe('Reservation is not completed :(');
        fireEvent.click(screen.getByTestId('toaster-close'));

        await loaded();
        expect(isSelected(id1)).toBe('true');
        expect(isReserved(id1)).toBe('false');
    });
});
