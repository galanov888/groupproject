import { DishType } from './dishes';

export type MenuItem = {
    count: number;
    product: DishType;
};

export type BasketItems = Record<string, MenuItem>;

export type Basket = {
    items: BasketItems;
    addItem: (input: DishType) => void;
    deleteItem: (input: DishType) => void;
    getCount: (input: DishType) => number;
    getAllOrderTotal: (input: DishType) => number;
    getAllOrderPrice: () => number;
    getAllCount: () => number;
    clear: () => void;
};
