import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../../store';
import { TabType } from '../../../data/reservation/tabtype';

export const reservationStateSelector = (state: RootState) => {
    return state.reservation;
};
export const tabsStateSelector = (state: RootState) =>
    reservationStateSelector(state).tabs;

export const getTabIds = createSelector(tabsStateSelector, (tabs) => {
    return tabs.map((el) => el.id);
});

export const getCurrentDate = (state: RootState) =>
    reservationStateSelector(state).currentDate;

export const selectTabById = createSelector(
    [(state: RootState) => state.reservation.tabs, (state, id) => id],
    (tabs, id) => tabs.find((el) => el.id === id)
);

export const isReservedById = createSelector(
    [
        (state: RootState) => state.reservation.tabs,
        getCurrentDate,
        (state, id) => id
    ],
    (tabs, currentDate, id) => {
        const tab = tabs.find((el) => el.id === id);
        if (tab) {
            for (const el of tab.timeStamps) {
                if (currentDate === el) {
                    return true;
                }
            }
        }
        return false;
    }
);

export const isInProgressById = createSelector(
    [
        (state: RootState) => state.reservation.tabs,
        getCurrentDate,
        (state, id) => id
    ],
    (tabs, currentDate, id) => {
        const tab = tabs.find((el) => el.id === id);
        if (tab) {
            for (const el of tab.chosenDates) {
                if (currentDate === el) {
                    return true;
                }
            }
        }
        return false;
    }
);

export const getLocationById = createSelector(
    [(state: RootState) => state.reservation.tabs, (state, id) => id],
    (tabs, id) => {
        const tab = tabs.find((el) => el.id === id);
        if (tab) {
            return tab.location;
        }
        return [];
    }
);
export const chosenTabIsPresent = createSelector(tabsStateSelector, (tabs) => {
    for (const tab of tabs) {
        if (tab.chosenDates.length) {
            return true;
        }
    }
    return false;
});

export const getConfirmMessage = createSelector(tabsStateSelector, (tabs) => {
    let message = '';
    for (const tab of tabs) {
        if (tab.chosenDates.length) {
            message += `${tab.id}: ${tab.chosenDates}\n`;
        }
    }
    return message;
});

export const getPostTabs = createSelector(tabsStateSelector, (tabs) => {
    const postTabs: TabType[] = [];
    for (const tab of tabs) {
        if (tab.chosenDates.length) {
            postTabs.push({
                id: tab.id,
                location: tab.location,
                timeStamps: tab.timeStamps.concat(tab.chosenDates)
            });
        }
    }
    return postTabs;
});
