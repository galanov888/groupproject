import React, { useContext } from 'react';
import {
    generatePath,
    useSearchParams,
    useLocation,
    useNavigate
} from 'react-router-dom';
import { getNavigationsValue } from '@ijl/cli';
import {
    StyledButton,
    StyledCartImage,
    StyledHeader,
    StyledNavLink
} from './header.style';
import ThemeToggle from '../themetoggle/themetoggle';
import { MyThemeContext } from '../../context/themeContext';
import { Day } from '../../theme';
import LoginForm from '../login/login';
import type { RootState } from '../../__data__/store';
import { useSelector } from 'react-redux';
import Basket from '../basket/basket';

export type UserURLSearchParams = URLSearchParams & { login: string };

const Header: React.FC = () => {
    const { theme } = useContext(MyThemeContext);
    const loginImg: string =
        theme === Day ? 'login_day.png' : 'login_night.png';
    const loginPath = `${__webpack_public_path__}/remote-assets/login/${loginImg}`;
    const [searchParams, setSearchParams] = useSearchParams();
    const { pathname } = useLocation();
    const navigate = useNavigate();
    const isAuthorized = useSelector(
        (state: RootState) => state.user.isAuthorized
    );
    const clickLogin = async () => {
        if (isAuthorized) {
            navigate(generatePath(getNavigationsValue('restaurant.account')));
        } else {
            if (pathname === getNavigationsValue('restaurant.registration')) {
                await navigate(
                    generatePath(
                        getNavigationsValue('restaurant.home') + '?login=true'
                    )
                );
            } else {
                setSearchParams((prev) => {
                    return {
                        ...prev,
                        login: true
                    };
                });
            }
        }
    };
    return (
        <>
            <StyledHeader>
                <StyledNavLink to={getNavigationsValue('restaurant.home')}>
                    home
                </StyledNavLink>
                <StyledNavLink to={getNavigationsValue('restaurant.menu')}>
                    menu
                </StyledNavLink>
                <StyledNavLink to={getNavigationsValue('restaurant.feedback')}>
                    feedback
                </StyledNavLink>
                <StyledNavLink
                    to={getNavigationsValue('restaurant.reservation')}
                >
                    reservation
                </StyledNavLink>
                <StyledButton onClick={clickLogin}>
                    <StyledCartImage src={loginPath} alt={'Login'} />
                </StyledButton>
                <Basket />
                <ThemeToggle />
            </StyledHeader>
            {searchParams.get('login') && (
                <LoginForm
                    handleClose={() =>
                        setSearchParams((prev: UserURLSearchParams) => {
                            const newParam = { ...prev };
                            delete newParam.login;
                            return newParam;
                        })
                    }
                ></LoginForm>
            )}
        </>
    );
};

export default Header;
