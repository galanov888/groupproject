import { getTitlePrice, get16BytePrice } from './../utils';

describe('utils', () => {
    it('getTitlePrice', () => {
        const result = getTitlePrice(244);
        expect(result).toBe('244 ₽');
    });
    it('get16BytePrice', () => {
        const result = get16BytePrice(244);
        expect(result).toBe('0xf4 ₽');
    });
});
