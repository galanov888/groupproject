import React from 'react';
import DishDataService from '../../data/dish/dishdataservice';
import { DishType } from '../../data/dishes';
import MenuCategory from './components/menu-category/menu-category';
import { Menu, MenuTitle } from './menu.style';

const MenuChoice: React.FC<{ dishData: DishType[] }> = ({ dishData }) => {
    const dishDataService = new DishDataService(dishData);
    const grouped = dishDataService.groupBy(dishData, 'category');
    return (
        <div className="menuChoice">
            <MenuTitle>Menu</MenuTitle>
            <Menu>
                {[...grouped.keys()].map((k) => {
                    return (
                        <MenuCategory
                            key={k}
                            category={k}
                            dishs={grouped.get(k)}
                        />
                    );
                })}
            </Menu>
        </div>
    );
};

export default MenuChoice;
