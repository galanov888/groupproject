import LoadingAnimationDark from './loading-dark.json';
import LoadingAnimationLight from './loading-light.json';

export { LoadingAnimationDark, LoadingAnimationLight };
