import React from 'react';
import { ThemeContextProvider } from '../../../context/themeContext';
import { render, screen } from '@testing-library/react';
import About from './../about';
describe('helloblcok', () => {
    it('helloblock snapshot', () => {
        const { container } = render(
            <ThemeContextProvider>
                <About />
            </ThemeContextProvider>
        );
        expect(container).toMatchSnapshot();
    });
});
