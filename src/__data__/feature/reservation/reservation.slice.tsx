import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getTimeStamp, TabType } from '../../../data/reservation/tabtype';

type choice = {
    chosenDates: string[];
};
export type TabSliceUpdate = {
    id: string;
    date: string;
};
export type TabSliceItem = TabType & choice;

export interface ReservationState {
    tabs: TabSliceItem[];
    currentDate: string;
}

const initialState: ReservationState = {
    tabs: [],
    currentDate: getTimeStamp(new Date())
};

export const reservationSlice = createSlice({
    name: 'tab',
    initialState,
    reducers: {
        setCurrentDate: (state, action: PayloadAction<string>) => {
            state.currentDate = action.payload;
        },
        addTab: (state, action: PayloadAction<TabType>) => {
            const item = state.tabs.find(
                (item) => item.id === action.payload.id
            );
            if (!item) {
                const t: TabSliceItem = {
                    ...action.payload,
                    chosenDates: []
                };
                state.tabs.push(t);
            }
        },
        addTimeStamp: (state, action: PayloadAction<TabSliceUpdate>) => {
            const item = state.tabs.find((el) => el.id === action.payload.id);
            if (item) {
                if (item.chosenDates.indexOf(action.payload.date) === -1) {
                    item.chosenDates.push(action.payload.date);
                }
            } else {
                //error
            }
        },
        removeTimeStamp: (state, action: PayloadAction<TabSliceUpdate>) => {
            const item = state.tabs.find(
                (item) => item.id === action.payload.id
            );
            if (item) {
                const index = item.chosenDates.indexOf(action.payload.date);
                if (index !== -1) {
                    item.chosenDates.splice(index, 1);
                }
            }
        },
        confirmReservation: (state) => {
            for (const tab of state.tabs) {
                if (tab.chosenDates.length) {
                    tab.timeStamps = tab.timeStamps.concat(tab.chosenDates);
                }
            }
        },
        clearChosenDates: (state) => {
            for (const tab of state.tabs) {
                if (tab.chosenDates.length) {
                    tab.chosenDates = [];
                }
            }
        },
        revertReservation: (state) => {
            for (const tab of state.tabs) {
                for (const cc of tab.chosenDates) {
                    const index = tab.timeStamps.indexOf(cc);
                    if (index !== -1) {
                        tab.timeStamps.splice(index, 1);
                    }
                }
            }
        }
    }
});

export const {
    setCurrentDate,
    addTimeStamp,
    removeTimeStamp,
    addTab,
    confirmReservation,
    clearChosenDates,
    revertReservation
} = reservationSlice.actions;
export default reservationSlice.reducer;
