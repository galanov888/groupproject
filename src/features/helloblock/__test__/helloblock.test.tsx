import React from 'react';
import { ThemeContextProvider } from '../../../context/themeContext';
import { render, screen } from '@testing-library/react';
import HelloBlock from './../helloblock';
describe('helloblcok', () => {
    it('helloblock snapshot', () => {
        const { container } = render(
            <ThemeContextProvider>
                <HelloBlock />
            </ThemeContextProvider>
        );
        expect(container).toMatchSnapshot();
    });
});
