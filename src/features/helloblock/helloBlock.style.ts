import styled from '@emotion/styled';

export const StyledDiv = styled.div`
    display: inline-block;
    flex-direction: row;
    align-items: center;
    background-color: #252525;
    height: 400px;
    width: 100%;
    position: relative;
`;

export const StyledDivForText = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;
