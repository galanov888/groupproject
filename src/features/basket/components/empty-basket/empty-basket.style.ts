import styled from '@emotion/styled';

export const EmptyBasketStyled = styled.div`
    text-align: center;
    color: ${(props) => props.theme.color.purple};
    font-size: ${(props) => props.theme.fontSize.l};
    font-family: ${(props) => props.theme.font.main};
`;
