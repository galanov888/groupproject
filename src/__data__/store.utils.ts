import { TypedUseSelectorHook, useSelector } from 'react-redux';
import { store } from './store';

export type RootState = ReturnType<typeof store.getState>;
export const rootSelector = (state: RootState) => state;

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
