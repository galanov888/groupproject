import { createSelector } from '@reduxjs/toolkit';
import { rootSelector } from '../store.utils';

const basketSliceSelector = createSelector(
    rootSelector,
    (state) => state.basket
);

export const basketSelector = (state) => {
    return basketSliceSelector(state).basketItems;
};

export const basketAllCountSelector = createSelector(
    basketSliceSelector,
    (slice) => {
        let total = 0;
        for (const i of Object.values(slice.basketItems)) {
            total += i.count;
        }
        return total;
    }
);

export const basketTotalPrice = createSelector(basketSliceSelector, (slice) => {
    let total = 0;
    for (const i of Object.values(slice.basketItems)) {
        total += i.count * i.product.price;
    }
    return total;
});

export const getCountItem = createSelector(
    basketSelector,
    (state, item) => item,
    (basketItems, item) =>
        basketItems[item.id] === undefined ? 0 : basketItems[item.id].count
);
