const errorHandlingMidleware = (error, req, res, next) => {
    console.error(error);
    res.status(500).send('Internal server error');
};
// eslint-disable-next-line no-undef
module.exports = errorHandlingMidleware;
