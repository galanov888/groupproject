import React, { useState } from 'react';
import { Basket, BasketItems } from '../data/menu';
import { DishType } from '../data/dishes';

// export const BasketContext = React.createContext<Basket>(null);

// export const BasketContextProvider: React.FC = (props) => {
//     const [basket, setBasket] = useState<BasketItems>({});

//     const addItem = (input: DishType) => {
//         const result = basket[input.id];
//         if (result === undefined) {
//             setBasket({
//                 ...basket,
//                 [input.id]: {
//                     count: 1,
//                     product: input
//                 }
//             });
//         } else {
//             setBasket({
//                 ...basket,
//                 [input.id]: {
//                     ...basket[input.id],
//                     count: basket[input.id].count + 1
//                 }
//             });
//         }
//     };

//     const deleteItem = (input: DishType) => {
//         const result = basket[input.id];

//         if (result !== undefined) {
//             if (result.count === 1) {
//                 const newBasket = { ...basket };
//                 delete newBasket[input.id];
//                 setBasket(newBasket);
//             } else {
//                 setBasket({
//                     ...basket,
//                     [input.id]: {
//                         ...basket[input.id],
//                         count: basket[input.id].count - 1
//                     }
//                 });
//             }
//         }
//     };

//     const getCount = (input: DishType) => {
//         const result = basket[input.id];
//         if (result !== undefined) {
//             return result.count;
//         } else {
//             return 0;
//         }
//     };

//     const getAllOrderTotal = () => {
//         let total = 0;
//         for (const i of Object.values(basket)) {
//             total += i.count;
//         }
//         return total;
//     };

//     const getAllOrderPrice = () => {
//         let total = 0;
//         for (const i of Object.values(basket)) {
//             total += i.count * i.product.price;
//         }
//         return total;
//     };

//     const getAllCount = () => {
//         let total = 0;
//         for (const i of Object.values(basket)) {
//             total += i.count;
//         }
//         return total;
//     };

//     const clear = () => {
//         setBasket({});
//     };

//     return (
//         <BasketContext.Provider
//             value={{
//                 items: basket,
//                 addItem: addItem,
//                 deleteItem: deleteItem,
//                 getCount: getCount,
//                 getAllOrderTotal: getAllOrderTotal,
//                 getAllOrderPrice: getAllOrderPrice,
//                 getAllCount: getAllCount,
//                 clear: clear
//             }}
//         >
//             {props.children}
//         </BasketContext.Provider>
//     );
// };
