import React from 'react';
import { DishType } from '../../../../data/dishes';
import { generatePath, Link } from 'react-router-dom';
import { getNavigationsValue } from '@ijl/cli';
import Counter from '../../../counter/counter';
import {
    SpanPrice,
    SpanTitle,
    StyledMenuItem,
    Decor,
    StyleCounter
} from './menu-item.style';

const MenuItem: React.FC<{ dish: DishType }> = ({ dish }) => {
    return (
        <StyledMenuItem>
            <Link
                to={generatePath(getNavigationsValue('restaurant.dish'), {
                    dishId: dish.id
                })}
                style={{ textDecoration: 'none' }}
                replace
            >
                <SpanTitle>
                    &quot;{dish.title}&quot;
                    <SpanPrice>: 0x{dish.price.toString(16)}₽</SpanPrice>
                </SpanTitle>
            </Link>
            <Decor>{`,`}</Decor>
            <StyleCounter>
                <Counter dish={dish} />
            </StyleCounter>
        </StyledMenuItem>
    );
};
export default MenuItem;
