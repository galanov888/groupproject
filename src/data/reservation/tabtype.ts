export enum TabState {
    reserved,
    free,
    inprogress
}

export type TabType = {
    id: string;
    location: number[][];
    timeStamps: string[];
};

export const getTimeStamp = (date: Date): string => {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
};
