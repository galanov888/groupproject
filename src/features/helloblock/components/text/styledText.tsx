import styled from '@emotion/styled';

export const StyledText = styled.p`
    flex-direction: row;
    align-items: center;
    color: white;
    margin: 0 20px;
    text-align: center;
    vertical-align: middle;
    margin-top: 20px;
    font-family: ${(props) => props.theme.font.main};
`;

export const StyledTextName = styled.h2`
    flex-direction: row;
    align-items: center;
    color: #00a438;
    margin: 0 20px;
    //font-family: ${(props) => props.theme.font.main};
    font-family: 'Iceland';
    font-style: normal;
    text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border: 1px solid #000000;
    font-weight: 200;
    font-size: 80px;
    text-align: center;
`;
