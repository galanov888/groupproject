import styled from '@emotion/styled';

export const StyledRecommendedTitle = styled.div`
    font-family: ${(props) => props.theme.font.main};
    align-content: center;
    color: ${(props) => props.theme.color.main};
    font-size: ${(props) => props.theme.fontSize.xs};
    padding-bottom: ${(props) => props.theme.spacing.xs};
`;

export const StyledRecommendedItem = styled.div`
    padding-right: ${(props) => props.theme.spacing.xl};
    border: 2px solid ${(props) => props.theme.color.debug};
`;
export const StyledDishPrice = styled.div`
    font-family: ${(props) => props.theme.font.main};
    color: ${(props) => props.theme.color.decor2};
    font-size: ${(props) => props.theme.fontSize.xs};
`;
