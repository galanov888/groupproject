import React from 'react';

import MainDish from '../../features/dishdetails/components/maindish/maindish';
import Recommendations from '../../features/dishdetails/components/recommendations/recommendations';
import { useParams } from 'react-router-dom';

import { useGetDishQuery, useGetAlsoLikeQuery } from '../../__data__/api';

const DishPage: React.FC = () => {
    const param = useParams();
    const dishRes = useGetDishQuery(param.dishId);
    const alsoLikeRes = useGetAlsoLikeQuery(param.dishId);

    let dishMessage: string;
    if (dishRes.isSuccess) {
        // console.log('dish ok');
    }
    if (dishRes.isError) {
        dishMessage = 'Dish is unavailable now :( ';
    }
    if (dishRes.isLoading) {
        dishMessage = 'Loading dish data...\n';
    }
    let alsoLikeMessage: string;
    if (alsoLikeRes.isError) {
        alsoLikeMessage = 'Recommendations are unavailable now :( ';
    }
    if (alsoLikeRes.isLoading) {
        alsoLikeMessage = 'Loading recommendations...\n';
    }

    return (
        <div>
            {dishRes.data ? (
                <MainDish dish={dishRes.data.data} />
            ) : (
                `${dishMessage}`
            )}
            {alsoLikeRes.data ? (
                <Recommendations category={alsoLikeRes.data.data} />
            ) : (
                `${alsoLikeMessage}`
            )}
        </div>
    );
};

export default DishPage;
