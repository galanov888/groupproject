import styled from '@emotion/styled';

export const StyledThemeToggle = styled.div`
    height: ${(props) => props.theme.spacing.m};
    font-size: ${(props) => props.theme.fontSize.s};
    font-family: ${(props) => props.theme.font.title};
    color: ${(props) => props.theme.color.header};
    border: 1px solid ${(props) => props.theme.color.header};
    border-radius: 3px;
    padding: 7px;
    margin-top: 15px;
    :hover {
        cursor: pointer;
    }
    justify-content: flex-end;
`;
