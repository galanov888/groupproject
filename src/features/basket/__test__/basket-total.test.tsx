import React from 'react';
import { render } from '@testing-library/react';
import BasketTotal from './../components/basket-total/basket-total';
import { ThemeContextProvider } from '../../../context/themeContext';
import { screen } from '@testing-library/react';
describe('basket-total', () => {
    it('basket-total is empty', () => {
        const { container } = render(
            <ThemeContextProvider>
                <BasketTotal total={0}></BasketTotal>
            </ThemeContextProvider>
        );

        const totalPrice = screen.getByText(/0x0 ₽/i);
        expect(totalPrice).not.toBeNull();
        expect(container).toMatchSnapshot();
    });

    it('basket-total is not empty', () => {
        const { container } = render(
            <ThemeContextProvider>
                <BasketTotal total={244}></BasketTotal>
            </ThemeContextProvider>
        );

        const totalPrice = screen.getByText(/0xf4 ₽/i);
        expect(totalPrice).not.toBeNull();
        expect(screen.getByText(/0xf4 ₽/i)).toBeInTheDocument();

        expect(container).toMatchSnapshot();
    });
});
