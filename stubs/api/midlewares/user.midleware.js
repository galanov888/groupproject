// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const jwt = require('jsonwebtoken');

const userMidleware = (req, res, next) => {
    let token = req.header['authorization'];
    if (token) {
        try {
            token = token.replace('Bearer ');
            const decodedToken = jwt.verify(token, 'secret');
            req.user = decodedToken;
        } catch (e) {
            console.error(e);
        }
    }
    next();
};

const authGuard = (req, res, next) => {
    if (!req.user) {
        res.status(403).send();
        return;
    }
    next();
};

// eslint-disable-next-line no-undef
module.exports = userMidleware;

// eslint-disable-next-line no-undef
module.exports = authGuard;
