import React from 'react';
import Basket from './../basket';
import { render, waitFor, screen, fireEvent } from '@testing-library/react';
import { ThemeContextProvider } from '../../../context/themeContext';
import { Provider } from 'react-redux';
import { setupStore } from './../../../__data__/store';
import basketStateMock from './basket.json';
import { setupServer } from 'msw/node';
import { rest } from 'msw';
import { jest } from '@jest/globals';
import { act } from 'react-dom/test-utils';

const server = setupServer(
    rest.post('/order', (req, res, ctx) => {
        console.log('post');
        return res(ctx.json({ orderNumber: '2003' }));
    })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('basket', () => {
    it.skip('basket is empty', () => {
        const store = setupStore();
        const { container } = render(
            <Provider store={store}>
                <ThemeContextProvider>
                    <Basket></Basket>
                </ThemeContextProvider>
            </Provider>
        );
        const emptyComponent = screen.getByText(/empty/i);
        expect(emptyComponent).not.toBeNull();
    });

    it.skip('basket is not empty', () => {
        const store = setupStore(basketStateMock);

        const { container, debug } = render(
            <Provider store={store}>
                <ThemeContextProvider>
                    <Basket></Basket>
                </ThemeContextProvider>
            </Provider>
        );
        const component = screen.queryByText(/0x0 ₽/i);
        expect(component).not.toBeInTheDocument();
        expect(screen.queryByText(/empty/i)).toBeNull();
    });

    it('basket send order', async () => {
        const store = setupStore(basketStateMock);
        window.alert = jest.fn();
        const { container, debug } = render(
            <Provider store={store}>
                <ThemeContextProvider>
                    <Basket></Basket>
                </ThemeContextProvider>
            </Provider>
        );
        const button = screen.getByRole('button', {
            name: /order/i
        });
        act(() => {
            fireEvent.click(button);
        });
        await waitFor(() =>
            expect(screen.queryByText(/empty/i)).not.toBeNull()
        );
        expect(window.alert).toBeCalledWith('Your order number: 2003');
        expect(window.alert).toHaveBeenCalledTimes(1);
    });
});
