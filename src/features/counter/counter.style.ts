import styled from '@emotion/styled';

export const StyledCounterButton = styled.div`
    font-family: ${(props) => props.theme.font.main};
    color: ${(props) => props.theme.color.decor1};
    font-size: ${(props) => props.theme.fontSize.s};
    border: 1px solid ${(props) => props.theme.color.decor1};
    border-radius: 3px;
    padding: 2px 5px;
    :hover {
        color: ${(props) => props.theme.color.decor1hover};
        border-color: ${(props) => props.theme.color.decor1hover};
        cursor: pointer;
    }
`;

export const StyledCounterBlock = styled.div`
    flex-wrap: wrap;
    display: flex;
`;

export const StyledQuantity = styled.div`
    font-family: ${(props) => props.theme.font.main};
    color: ${(props) => props.theme.color.decor1};
    font-size: ${(props) => props.theme.fontSize.s};
    margin-left: 7px;
    margin-right: 7px;
`;
