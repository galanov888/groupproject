export enum DishCategories {
    Salads = 'Salads',
    CFood = 'C-Food',
    Soups = 'Soups',
    Desserts = 'Desserts',
    Drinks = 'Drinks',
    HotDish = 'Hot Dish'
}
export type DishType = {
    id: string;
    category: DishCategories;
    title: string;
    image: string;
    price: number;
    description: string;
};
