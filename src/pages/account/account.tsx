import React from 'react';
import type { RootState } from '../../__data__/store';
import { useSelector, useDispatch } from 'react-redux';
import { signOut } from '../../__data__/feature/user/user.slice';
import { generatePath, useNavigate } from 'react-router-dom';
import { getNavigationsValue } from '@ijl/cli';

const Account: React.FC = () => {
    const email = useSelector((state: RootState) => state.user.email);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    return (
        <div>
            <div>
                <div>Email : {email}</div>
                <button
                    aria-label="Sign Out"
                    onClick={() => {
                        dispatch(signOut());
                        navigate(
                            generatePath(getNavigationsValue('restaurant.home'))
                        );
                    }}
                >
                    Sign Out
                </button>
            </div>
        </div>
    );
};

export default Account;
