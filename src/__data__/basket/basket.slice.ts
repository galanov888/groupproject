import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { DishType } from '../../../src/data/dishes';
import { BasketItems } from '../../../src/data/menu';

interface BasketState {
    basketItems: BasketItems;
}

const initialState: BasketState = {
    basketItems: {}
};

const basketSlice = createSlice({
    name: 'basket',
    initialState,
    reducers: {
        addItem: (state, action: PayloadAction<DishType>) => {
            const result = state.basketItems[action.payload.id];
            if (result === undefined) {
                state.basketItems = {
                    ...state.basketItems,
                    [action.payload.id]: {
                        count: 1,
                        product: action.payload
                    }
                };
            } else {
                state.basketItems = {
                    ...state.basketItems,
                    [action.payload.id]: {
                        ...state.basketItems[action.payload.id],
                        count: state.basketItems[action.payload.id].count + 1
                    }
                };
            }
        },
        deleteItem: (state, action: PayloadAction<DishType>) => {
            const result = state.basketItems[action.payload.id];
            if (result !== undefined) {
                if (result.count === 1) {
                    const basketItems = { ...state.basketItems };
                    delete basketItems[action.payload.id];
                    state.basketItems = basketItems;
                } else {
                    state.basketItems = {
                        ...state.basketItems,
                        [action.payload.id]: {
                            ...state.basketItems[action.payload.id],
                            count:
                                state.basketItems[action.payload.id].count - 1
                        }
                    };
                }
            }
        },
        clear: (state) => {
            state.basketItems = {};
        }
    }
});

export const { reducer: basketReducer, actions: basketActions } = basketSlice;
