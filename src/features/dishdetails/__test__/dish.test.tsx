import * as React from 'react';
import {
    describe,
    it,
    expect,
    beforeAll,
    afterEach,
    afterAll
} from '@jest/globals';
import { render, waitFor, screen, fireEvent } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import '@testing-library/jest-dom';
import data from './dishdata.test.json';

import { Provider } from 'react-redux';
import { store } from '../../../__data__/store';
import 'whatwg-fetch';
import { ThemeContextProvider } from '../../../context/themeContext';
import { BrowserRouter } from 'react-router-dom';
import DishPage from '../../../pages/dish';

const server = setupServer(
    rest.get('/dish/:dishId', (req, res, ctx) => {
        return res(ctx.json({ data: data[0] }));
    }),
    rest.get('/dish/alsoLike/:dishId', (req, res, ctx) => {
        return res(ctx.json({ data: data.slice(1, 4) }));
    })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('Dish Components Tests', () => {
    jest.mock('react-router', () => ({
        useParams: jest.fn().mockReturnValue({ dishId: 's1' })
    }));
    it('Main dish and all recommended dishes are present', async () => {
        const { container } = render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <BrowserRouter>
                        <DishPage />
                    </BrowserRouter>
                </Provider>
            </ThemeContextProvider>
        );

        await waitFor(() => screen.getByTestId('maindish-s1'));
        await waitFor(() => screen.getByTestId('alsolike-s2'));
        await waitFor(() => screen.getByTestId('alsolike-s3'));
        await waitFor(() => screen.getByTestId('alsolike-s4'));

        expect(container).toMatchSnapshot();
    });

    it('Check counter for main dish', async () => {
        render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <BrowserRouter>
                        <DishPage />
                    </BrowserRouter>
                </Provider>
            </ThemeContextProvider>
        );

        await waitFor(() => screen.getByTestId('counter-add'));
        fireEvent.click(screen.getByTestId('counter-add'));
        await waitFor(() => screen.getByTestId('counter-plus'));
        await waitFor(() => screen.getByTestId('counter-minus'));
        expect(screen.getByTestId('counter-count').textContent).toBe('1');
        fireEvent.click(screen.getByTestId('counter-plus'));
        expect(screen.getByTestId('counter-count').textContent).toBe('2');
        fireEvent.click(screen.getByTestId('counter-minus'));
        expect(screen.getByTestId('counter-count').textContent).toBe('1');
        fireEvent.click(screen.getByTestId('counter-minus'));
        expect(screen.getByTestId('counter-add')).toBeTruthy();
    });
});
