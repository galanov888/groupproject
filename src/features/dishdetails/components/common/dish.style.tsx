import styled from '@emotion/styled';

export const StyledDishImage = styled.img<{ small: boolean }>`
    width: ${(props) => (props.small ? 128 : 256)}px;
    height: ${(props) => (props.small ? 128 : 256)}px;
    border: 2px solid ${(props) => props.theme.color.decor4};
    border-radius: 3px;
`;
