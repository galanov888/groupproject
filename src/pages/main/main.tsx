import React from 'react';
import About from '../../features/aboutblock/about';
import HelloBlock from '../../features/helloblock/helloBlock';

const Main: React.FC = () => {
    return (
        <>
            <HelloBlock />
            <About />
        </>
    );
};

export default Main;
