import React, { useEffect, useState } from 'react';
import { StyledFeedback } from './feedback.style';

const FeedbackTextRevert =
    '.илатед и йещев ьтус ьтинсяъбо ,ьчомоп ьтсонвотог ,еинешонто еоньлеталежорбод аз )ысорпов есв ан иксечиткарп ытевто' +
    ' теанз йыроток( увеьрогирГ етикиН уротнем умешан обисапс еоньледтО ' +
    '.иицкел еынсеретни аз мялетавадоперп месв и ьтсонжомзов юуннелватсодерп аз асрук маротазинагро обисапС';

const feedbackText = FeedbackTextRevert.split('').reverse().join('');
const Feedback = () => {
    const [counter, setCounter] = useState(0);
    useEffect(() => {
        const timer = setTimeout(() => {
            if (counter < feedbackText.length) {
                setCounter(counter + 1);
            }
        }, 25);
        return () => {
            clearTimeout(timer);
        };
    }, [counter]);

    return (
        <StyledFeedback data-testid={'feedback'}>
            <i>{feedbackText.slice(0, counter)}</i>
        </StyledFeedback>
    );
};
export default Feedback;
