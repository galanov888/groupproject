import React from 'react';
import { describe, it, expect } from '@jest/globals';
import { render, screen } from '@testing-library/react';

import MenuChoice from '../index';
import { DishData } from './dish-data-test';

import { ThemeContextProvider } from '../../../context/themeContext';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../../__data__/store';

describe('Menu', () => {
    it('snapshot', () => {
        const { container } = render(
            <ThemeContextProvider>
                <BrowserRouter>
                    <Provider store={store}>
                        <MenuChoice dishData={DishData} />
                    </Provider>
                </BrowserRouter>
            </ThemeContextProvider>
        );
        expect(container).toMatchSnapshot();
        const categories = screen.queryAllByLabelText('category');
        expect(categories).toHaveLength(4);
        expect(screen.getByText(/"Salads":/i)).toBeTruthy();
        expect(screen.getByText(/"C-Food":/i)).toBeTruthy();
        expect(screen.getByText(/"Soups":/i)).toBeTruthy();
        expect(screen.getByText(/"Desserts":/i)).toBeTruthy();
    });
});
