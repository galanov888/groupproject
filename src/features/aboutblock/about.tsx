import React from 'react';
import { vipText } from '../../data/main/text';
import {
    StyledAboutDiv,
    StyledAboutImg,
    StyledAboutText
} from './about-block.style';

const About: React.FC = () => {
    return (
        <>
            <StyledAboutDiv>
                <StyledAboutImg
                    src={`${__webpack_public_path__}/remote-assets/main/vip.jpg`}
                />
                <StyledAboutText>{vipText}</StyledAboutText>
            </StyledAboutDiv>
        </>
    );
};

export default About;
