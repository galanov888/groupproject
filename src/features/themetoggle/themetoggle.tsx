import React, { useContext } from 'react';
import { Day, Night } from '../../theme';
import { MyThemeContext } from '../../context/themeContext';
import { StyledThemeToggle } from './themetoggle.style';

const ThemeToggle = () => {
    const { theme, setTheme } = useContext(MyThemeContext);
    const mode: string = theme === Day ? 'dark mode' : 'light mode';
    const handle = () => {
        if (theme === Day) {
            setTheme(Night);
        } else {
            setTheme(Day);
        }
    };

    return <StyledThemeToggle onClick={handle}>{mode}</StyledThemeToggle>;
};

export default ThemeToggle;
