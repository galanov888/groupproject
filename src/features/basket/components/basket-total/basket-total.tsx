import React from 'react';
import {
    StyledDivInLine,
    StyledText,
    StyledTextPrice
} from '../basket-item/basket-item.style';
import { StyledOrderButton } from '../order-button/order-button.style';
import { getTitlePrice, get16BytePrice } from '../../utils';

type BasketTotalProps = {
    total: number;
};

const BasketTotal: React.FC<BasketTotalProps> = ({ total }) => {
    return (
        <StyledDivInLine>
            <StyledText>Итого:</StyledText>
            <StyledTextPrice title={getTitlePrice(total)}>
                {get16BytePrice(total)}
            </StyledTextPrice>
            <StyledOrderButton type="submit">Order</StyledOrderButton>
        </StyledDivInLine>
    );
};

export default BasketTotal;
