import styled from '@emotion/styled';

type DrawerProps = {
    isHidden: boolean;
};

export const DrawerContent = styled.div<DrawerProps>`
    height: 500px;
    width: 400px;
    width: ${(props) => (props.isHidden ? '0' : '400')}px;
    background-color: #cbc6bb;
    transition: all 0.3s ease-in-out;
    position: absolute;
    right: 0;
    overflow: auto;
    z-index: 99;
`;

export const Line = styled.hr`
    margin: 20px 0;
    padding: 0;
    height: 10px;
    border: none;
    border-top: 1px solid #333;
    box-shadow: 0 10px 10px -10px #8c8b8b inset;
`;
