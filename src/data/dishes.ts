export enum DishCategories {
    Salads = 'Salads',
    CFood = 'C-Food',
    Soups = 'Soups',
    Desserts = 'Desserts',
    Drinks = 'Drinks',
    HotDish = 'Hot Dish'
}
export type DishType = {
    id: string;
    category: DishCategories;
    title: string;
    image: string;
    price: number;
    description: string;
};

export const DishData: DishType[] = [
    {
        id: 's1',
        category: DishCategories.Salads,
        title: 'Qwerty salad',
        image: 'qwertysalad.jpg',
        price: 256,
        description:
            'Fragrant crispy keys dressed with coffee, beer or coca-cola'
    },
    {
        id: 's2',
        category: DishCategories.Salads,
        title: 'Fibonacci salad',
        image: 'fibonaccisalad.jpg',
        price: 150,
        description:
            'Refined taste of yesterday salad, complimented by salad from two days ago'
    },
    {
        id: 's3',
        category: DishCategories.Salads,
        title: 'RNG salad',
        image: 'rngsalad.jpg',
        price: 128,
        description:
            'Exquisite original taste of fully natural, randomly generated numbers'
    },
    {
        id: 's4',
        category: DishCategories.Salads,
        title: 'Merge request',
        image: 'mergerequestsalad.jpg',
        price: 256,
        description: 'Non-merged salad, ideal for merge conflicts intolerance'
    },
    {
        id: 'cf1',
        category: DishCategories.CFood,
        title: 'SalmON',
        image: 'salmon.jpg',
        price: 512,
        description: 'Salmon with user-friendly interface'
    },
    {
        id: 'cf2',
        category: DishCategories.CFood,
        title: 'SalmOFF',
        image: 'salmoff1.jpg',
        price: 4,
        description:
            'The pink Salmon skeleton is perfect for research and debugging'
    },
    {
        id: 'cf3',
        category: DishCategories.CFood,
        title: 'Develobster',
        image: 'develobster.jpg',
        price: 1024,
        description: 'High skilled senior frontend develobster'
    },
    {
        id: 'cf4',
        category: DishCategories.CFood,
        title: '8-bit Octopus',
        image: 'octopus1.jpg',
        price: 500,
        description: '8-bit Octopus summary'
    },
    {
        id: 'sp1',
        category: DishCategories.Soups,
        title: 'Hooks and props',
        image: 'hookspropssoup.jpg',
        price: 404,
        description:
            'A rich fragrant soup with a large set of properties and states that is easy to hook'
    },
    {
        id: 'sp2',
        category: DishCategories.Soups,
        title: 'Float points',
        image: 'floatpointsoup.jpg',
        price: 403,
        description:
            'A light soup, the original taste and unique aroma of which is provided by floating points'
    },
    {
        id: 'sp3',
        category: DishCategories.Soups,
        title: 'BorscHTML',
        image: 'borscht1.jpg',
        price: 401,
        description:
            'Marked up by sour cream soup based on thoroughly tested bit-root'
    },
    {
        id: 'sp4',
        category: DishCategories.Soups,
        title: 'Try-catch',
        image: 'trycatch3.jpg',
        price: 400,
        description:
            'Soup for real gourmets familiar with advanced try-catch technologies'
    }
];
