import React from 'react';
import { TabState } from '../../../../data/reservation/tabtype';
import { StyledPolygon, StyledSvg } from './tab.style';
import { useDispatch, useSelector } from 'react-redux';
import {
    addTimeStamp,
    removeTimeStamp
} from '../../../../__data__/feature/reservation/reservation.slice';

import {
    getCurrentDate,
    getLocationById,
    isInProgressById,
    isReservedById
} from '../../../../__data__/feature/reservation/reservation.selectors';

type TabProps = {
    tabId: string; //TabType;
};

const Tab: React.FC<TabProps> = ({ tabId }) => {
    const dispatch = useDispatch();
    const reserved = useSelector((state) => isReservedById(state, tabId));
    const currentDate = useSelector(getCurrentDate);
    const inProgress = useSelector((state) => isInProgressById(state, tabId));
    const location = useSelector((state) => getLocationById(state, tabId));

    const handle = () => {
        if (reserved) {
            return;
        }
        if (inProgress) {
            dispatch(removeTimeStamp({ id: tabId, date: currentDate }));
        } else {
            dispatch(addTimeStamp({ id: tabId, date: currentDate }));
        }
    };

    const scale = 20;
    //const tr = `rotate(${Math.floor(Math.random() * 20 - 10)})`;
    //transform={tr}
    return (
        <div>
            <StyledSvg
                key={tabId}
                data-testid={tabId}
                data-selected={inProgress}
                data-reserved={reserved}
            >
                {location.map((row, y) =>
                    row.map((item, x) =>
                        item ? (
                            <StyledPolygon
                                key={x + y * row.length + tabId}
                                state={
                                    reserved
                                        ? TabState.reserved
                                        : inProgress
                                        ? TabState.inprogress
                                        : TabState.free
                                }
                                x={x * scale}
                                y={y * scale}
                                width={scale}
                                height={scale}
                                onClick={handle}
                                data-testid={tabId + x + y}
                            />
                        ) : (
                            <div key={x + y * row.length + tabId}></div>
                        )
                    )
                )}
            </StyledSvg>
        </div>
    );
};
export default Tab;
