import styled from '@emotion/styled';
export const StyledDiv = styled.div`
    display: flex;
    justify-content: center;
`;

export const StyledReservationContainer = styled.div`
    padding-top: 80px;
`;
export const StyledReserve = styled.div`
    font-family: ${(props) => props.theme.font.main};
    font-size: ${(props) => props.theme.fontSize.s};
    color: #252525;
    width: 120px;
    margin-left: 100px;
    margin-top: 30px;
    border: ${(props) => props.theme.color.decor1} solid 3px;
    border-radius: 5px;
    display: flex;
    justify-content: center;
    background-image: linear-gradient(
        to right,
        ${(props) => props.theme.color.decor1},
        yellow
    );
    :hover {
        cursor: pointer;
    }
`;
