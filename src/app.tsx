import React from 'react';
import Menu from './pages/menu/menu';
import DishPage from './pages/dish';
import Layout from './pages/layout';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { getNavigationsValue } from '@ijl/cli';
import Main from './pages/main/main';
import { ThemeContextProvider } from './context/themeContext';
import Registration from './pages/registration';
import ReservationPage from './pages/reservation';
import { Provider } from 'react-redux';
import { store } from './__data__/store';
import Account from './pages/account/account';
import { FeedbackPage } from './pages/feedback';

const router = createBrowserRouter(
    [
        {
            path: '/',
            element: <Layout />,
            errorElement: <h1>Error</h1>,
            children: [
                {
                    path: getNavigationsValue('restaurant.home'),
                    element: <Main />
                },
                {
                    path: getNavigationsValue('restaurant.menu'),
                    element: <Menu />
                },
                {
                    path: getNavigationsValue('restaurant.dish'),
                    element: <DishPage />
                },
                {
                    path: getNavigationsValue('restaurant.feedback'),
                    element: <FeedbackPage />
                },
                {
                    path: getNavigationsValue('restaurant.reservation'),
                    element: <ReservationPage />
                },
                {
                    path: getNavigationsValue('restaurant.cart'),
                    element: <h1> Cart</h1>
                },
                {
                    path: getNavigationsValue('restaurant.registration'),
                    element: <Registration />
                },
                {
                    path: getNavigationsValue('restaurant.account'),
                    element: <Account />
                }
            ]
        }
    ],
    { basename: getNavigationsValue('restaurant.main') }
);
document.title = 'Restaurant Byte a Bit';
const App = () => {
    return (
        <Provider store={store}>
            <ThemeContextProvider>
                <RouterProvider router={router}></RouterProvider>
            </ThemeContextProvider>
        </Provider>
    );
};
export default App;
