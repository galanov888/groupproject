import * as React from 'react';
import {
    describe,
    it,
    expect,
    beforeAll,
    afterEach,
    afterAll
} from '@jest/globals';
import { render, waitFor, screen, fireEvent } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import '@testing-library/jest-dom';
import data from './reservation.test.json';
import { Provider } from 'react-redux';
import { setupStore } from '../../../__data__/store';
import 'whatwg-fetch';
import { ThemeContextProvider } from '../../../context/themeContext';
import { BrowserRouter } from 'react-router-dom';
import ReservationPage from '../../../pages/reservation';

const server = setupServer(
    rest.get('/reservation/', (req, res, ctx) => {
        return res(ctx.json(data));
    }),

    rest.post('/reservation/post', (req, res, ctx) => {
        return res(ctx.status(200), ctx.json(data));
    })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('Reservation Component Tests', () => {
    const isReserved = (id: string) =>
        screen.getByTestId(id).getAttribute('data-reserved');
    const isSelected = (id: string) =>
        screen.getByTestId(id).getAttribute('data-selected');
    const calendarInput =
        'div[class="react-datepicker__input-container"] input';
    const testDate = '2024-3-17';
    const testDate2 = '2025-12-17';
    const setCalendarDate = async (container: HTMLElement, date: string) => {
        await waitFor(() => container.querySelector(calendarInput));
        fireEvent.change(container.querySelector(calendarInput), {
            target: { value: date }
        });
    };
    const loaded = async () => {
        await waitFor(() => screen.getByTestId('t1'));
    };

    it('Test1: Table should be selected/unselected by click', async () => {
        const store = setupStore();
        const id = 't2';
        const rec = 't200';
        const { container } = render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <BrowserRouter>
                        <ReservationPage />
                    </BrowserRouter>
                </Provider>
            </ThemeContextProvider>
        );
        await loaded();
        await setCalendarDate(container, testDate);

        expect(isSelected(id)).toBe('false');
        await waitFor(() => screen.getByTestId(rec));
        fireEvent.click(screen.getByTestId(rec));
        expect(isSelected(id)).toBe('true');
        fireEvent.click(screen.getByTestId(rec));
        expect(isSelected(id)).toBe('false');
        expect(container).toMatchSnapshot();
    });

    it('Test2: Reservation when no tables are selected should open choose a table notification', async () => {
        const store = setupStore();
        const { container } = render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <BrowserRouter>
                        <ReservationPage />
                    </BrowserRouter>
                </Provider>
            </ThemeContextProvider>
        );
        await loaded();
        await setCalendarDate(container, testDate);

        fireEvent.click(screen.getByTestId('reservation'));
        await waitFor(() => screen.getByTestId('toaster-message'));
        const text = screen.getByTestId('toaster-message').textContent;
        expect(text).toBe('Choose a table');
        fireEvent.click(screen.getByTestId('toaster-close'));
    });

    it('Test3: Confirm Reservation modal has correct message', async () => {
        const store = setupStore();
        const { container } = render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <BrowserRouter>
                        <ReservationPage />
                    </BrowserRouter>
                </Provider>
            </ThemeContextProvider>
        );
        const id = 't3';
        const rec = 't300';

        await loaded();
        await setCalendarDate(container, testDate);
        expect(isReserved(id)).toBe('false');
        fireEvent.click(screen.getByTestId(rec));
        expect(isSelected(id)).toBe('true');
        expect(isReserved(id)).toBe('false');
        fireEvent.click(screen.getByTestId('reservation'));

        await waitFor(() => screen.getByTestId('modal-message'));
        const text = screen.getByTestId('modal-message').textContent;
        const text2 = text.replace(/\s+|\n/g, '');

        expect(text2).toBe(`ConfirmReservation:${id}:${testDate}`);
        fireEvent.click(screen.getByTestId('modal-close'));
        fireEvent.click(screen.getByTestId(rec));
        expect(isSelected(id)).toBe('false');
    });

    it('Test4: Selection is correct for different dates', async () => {
        const store = setupStore();
        const { container } = render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <BrowserRouter>
                        <ReservationPage />
                    </BrowserRouter>
                </Provider>
            </ThemeContextProvider>
        );
        const id = 't4';
        const rec = 't400';

        await loaded();
        await setCalendarDate(container, testDate);

        await waitFor(() => screen.getByTestId(id));

        expect(isReserved(id) && isSelected(id)).toBe('false');
        fireEvent.click(screen.getByTestId(rec));
        expect(isSelected(id)).toBe('true');

        await setCalendarDate(container, testDate2);
        await loaded();
        expect(isSelected(id)).toBe('false');
        await setCalendarDate(container, testDate);
        await loaded();
        expect(isSelected(id)).toBe('true');
    });

    it('Test5: Reservation is correct for different dates', async () => {
        const store = setupStore();
        const { container } = render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <BrowserRouter>
                        <ReservationPage />
                    </BrowserRouter>
                </Provider>
            </ThemeContextProvider>
        );
        const id1 = 't4';
        const rec1 = 't400';
        const id2 = 't5';
        const rec2 = 't500';

        await loaded();
        await setCalendarDate(container, testDate);

        await waitFor(() => screen.getByTestId(id1));

        expect(isReserved(id1) && isSelected(id1)).toBe('false');
        fireEvent.click(screen.getByTestId(rec1));
        await waitFor(() => screen.getByTestId(id1));
        expect(isSelected(id1)).toBe('true');

        expect(isReserved(id2) && isSelected(id2)).toBe('false');
        fireEvent.click(screen.getByTestId(rec2));
        expect(isSelected(id2)).toBe('true');

        await setCalendarDate(container, testDate2);
        await loaded();
        expect(isSelected(id1)).toBe('false');
        fireEvent.click(screen.getByTestId(rec1));
        await waitFor(() => screen.getByTestId(id1));
        expect(isSelected(id1)).toBe('true');

        fireEvent.click(screen.getByTestId('reservation'));
        await waitFor(() => screen.getByTestId('modal-message'));
        fireEvent.click(screen.getByTestId('modal-ok'));

        await waitFor(() => screen.getByTestId('toaster-message'));
        const text = screen.getByTestId('toaster-message').textContent;
        expect(text).toBe('Reservation completed successfully!');
        fireEvent.click(screen.getByTestId('toaster-close'));

        await loaded();
        expect(isSelected(id1)).toBe('false');
        expect(isReserved(id1)).toBe('true');
        expect(isSelected(id2)).toBe('false');
        expect(isReserved(id2)).toBe('false');
        fireEvent.click(screen.getByTestId(rec1));
        await waitFor(() => screen.getByTestId(id1));
        expect(isSelected(id1)).toBe('false');

        await setCalendarDate(container, testDate);
        await loaded();
        expect(isSelected(id1)).toBe('false');
        expect(isReserved(id1)).toBe('true');
        expect(isSelected(id2)).toBe('false');
        expect(isReserved(id2)).toBe('true');
    });
});
