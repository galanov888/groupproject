const checkRequiredMidleware = (options) => (req, res, next) => {
    console.log(req.body);
    for (const option of options) {
        if (!req.body[option]) {
            res.status(400).send(`Required field ${option} is not present`);
            return;
        }
    }
    next();
};

// eslint-disable-next-line no-undef
module.exports = checkRequiredMidleware;
