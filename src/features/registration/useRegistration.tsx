import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const apiUrl = getConfigValue('restaurant.apiUrl');

const UseRegistration = async (input) => {
    const submitRequest = async (input) => {
        try {
            const res = await axios({
                method: 'post',
                url: `${apiUrl}/auth/register`,
                data: input
            });
            const data = await res.data;
            return { response: data.data, error: data.error };
        } catch (ex) {
            return { response: undefined, error: ex.response.data };
        }
    };

    return await submitRequest(input);
};

export default UseRegistration;
