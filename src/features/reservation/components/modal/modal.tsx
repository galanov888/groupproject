import React from 'react';
import ReactDOM from 'react-dom';
import {
    ModalOverlay,
    ModalContainer,
    ModalOk,
    ModalClose,
    ModalCancel,
    ModalMessage
} from './modal.style';

type ModalProps = {
    isOpen: boolean;
    title?: string;
    message?: string;
    handleOk?: () => void;
    handleClose?: () => void;
};
const Modal: React.FC<ModalProps> = ({
    isOpen,
    title,
    handleClose,
    handleOk
}) => {
    if (!isOpen) return null;
    return ReactDOM.createPortal(
        <ModalOverlay>
            <ModalContainer>
                <ModalMessage data-testid={'modal-message'}>
                    {title.split('\n').map((item, index) => (
                        <div key={index}>
                            {item}
                            {'\n'}
                        </div>
                    ))}
                </ModalMessage>
                <ModalCancel onClick={handleClose} data-testid={'modal-cancel'}>
                    Cancel
                </ModalCancel>
                <ModalOk data-testid={'modal-ok'} onClick={handleOk}>
                    OK
                </ModalOk>
                <ModalClose onClick={handleClose} data-testid={'modal-close'}>
                    X
                </ModalClose>
            </ModalContainer>
        </ModalOverlay>,
        document.body
    );
};
export default Modal;
