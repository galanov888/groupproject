import React, { useContext } from 'react';
import { MenuItem } from '../../data/menu';
import BasketItem from './components/basket-item/basket-item';
import { StyledInputButton } from './components/basket-item/basket-item.style';
import BasketTotal from './components/basket-total/basket-total';
import { Day } from '../../theme';

import { MyThemeContext } from '../../context/themeContext';

import { DrawerContent, Line } from './basket.style';
import { StyledCartImage } from '../header/header.style';
import EmptyBasket from './components/empty-basket/empty-basket';
import { useAppSelector } from '../../__data__/store.utils';
import {
    basketAllCountSelector,
    basketSelector,
    basketTotalPrice
} from '../../__data__/basket/basket.selectors';
import { useDispatch } from 'react-redux';
import { basketActions } from '../../__data__/basket/basket.slice';
import { useAddOrderMutation } from '../../__data__/api';

const Basket: React.FC = () => {
    const anchor = 'right';

    const { theme } = useContext(MyThemeContext);
    const img: string = theme === Day ? 'cart_day.jpg' : 'cart_night.jpg';

    const imgPath = `${__webpack_public_path__}/remote-assets/dish/${img}`;
    const [state, setState] = React.useState({
        right: true
    });
    const toggleDrawer = (open: boolean) => (event: React.MouseEvent) => {
        setState({ ...state, [anchor]: open });
    };

    const [orderPost, { isError, isSuccess }] = useAddOrderMutation();

    const dispatch = useDispatch();
    const handleSubmit = async (event) => {
        event.preventDefault();
        if (allCount === 0) {
            alert('Basket is empty');
        } else {
            orderPost({
                userId: '123',
                products: []
            })
                .then((response) => {
                    console.log('get post');
                    if ('data' in response) {
                        console.log('in data');
                        alert(
                            `Your order number: ${response.data.orderNumber}`
                        );
                        console.log(response);
                        dispatch(basketActions.clear());
                    } else {
                        console.log('not data');
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    };

    const allCount = useAppSelector(basketAllCountSelector);
    const basketItems = useAppSelector(basketSelector);
    const totalPrice = useAppSelector(basketTotalPrice);
    return (
        <>
            <StyledCartImage
                onClick={toggleDrawer(false)}
                src={imgPath}
                alt={'Cart'}
            />
            <DrawerContent isHidden={state.right}>
                <form title="orderForm" onSubmit={handleSubmit}>
                    <StyledInputButton
                        type="button"
                        onClick={toggleDrawer(true)}
                    >
                        x
                    </StyledInputButton>
                    {allCount > 0 ? (
                        <BasketItemsList items={basketItems}></BasketItemsList>
                    ) : (
                        <EmptyBasket>Empty</EmptyBasket>
                    )}

                    <Line />
                    <BasketTotal total={totalPrice} />
                </form>
            </DrawerContent>
        </>
    );
};

export default Basket;

type BasketItemsListDrops = {
    items: Record<string, MenuItem>;
};
const BasketItemsList: React.FC<BasketItemsListDrops> = ({ items }) => {
    let myKey = 0;
    const listItems = Object.values(items).map((item) => {
        myKey += 1;

        return (
            <BasketItem item={item.product} key={item.product.id || myKey} />
        );
    });

    return <>{listItems}</>;
};
