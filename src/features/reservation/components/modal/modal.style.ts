import styled from '@emotion/styled';

export const ModalOverlay = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    //background: rgba(255, 255, 255, 2%);
    backdrop-filter: blur(1px);
`;

export const ModalContainer = styled.div`
    opacity: 0.9;
    position: fixed;
    top: 50%;
    left: 50%;
    width: 300px;
    height: 200px;
    transform: translate(-50%, -50%);
    background: ${(props) => props.theme.color.background};
    padding: 10px;
    border: 2px solid ${(props) => props.theme.color.decor4};
    border-radius: 5px;
    overflow-x: scroll;
    overflow-y: scroll;
    box-shadow: 0 0 5px ${(props) => props.theme.color.decor4};
`;

export const ModalMessage = styled.div`
    font-family: ${(props) => props.theme.font.main};
    font-size: ${(props) => props.theme.fontSize.xs};
    color: ${(props) => props.theme.color.decor3};
    padding-top: 10px;
`;

export const ModalOk = styled.button`
    cursor: pointer;
    position: absolute;
    left: calc(100% - 50px);
    top: calc(100% - 40px);
`;

export const ModalCancel = styled.button`
    cursor: pointer;
    position: absolute;
    left: calc(100% - 120px);
    top: calc(100% - 40px);
`;

export const ModalClose = styled.button`
    font-size: ${(props) => props.theme.fontSize.xs};
    cursor: pointer;
    position: absolute;
    left: calc(100% - 25px);
    bottom: calc(100% - 25px);
    background: ${(props) => props.theme.color.background};
    color: ${(props) => props.theme.color.main};
    width: 25px;
    height: 25px;
    border: none;
    z-index: 1;
`;
