import React from 'react';
import { StyledDiv, StyledDivForText } from './helloBlock.style';
import { StyledImg } from './components/image/image';
import { StyledText, StyledTextName } from './components/text/styledText';
import { restaurantName, helloText } from '../../data/main/text';

const HelloBlock: React.FC = () => {
    return (
        <>
            <StyledDiv>
                <StyledImg
                    src={`${__webpack_public_path__}/remote-assets/hello/hello.jpg`}
                />
                <StyledDivForText>
                    <StyledTextName>{restaurantName}</StyledTextName>
                    <StyledText>{helloText}</StyledText>
                </StyledDivForText>
            </StyledDiv>
        </>
    );
};

export default HelloBlock;
