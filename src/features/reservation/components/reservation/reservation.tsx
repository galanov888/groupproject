import React, { useEffect, useState } from 'react';

import Calendar from '../calendar/calendar';
import {
    StyledDiv,
    StyledReservationContainer,
    StyledReserve
} from './reservation.style';
import Legend from '../legend/legend';
import ChooseTable from '../choosetabele/choosetable';
import Hall from '../hall/hall';
import { usePostTabsMutation } from '../../../../__data__/api';
import { useDispatch, useSelector } from 'react-redux';
import {
    getPostTabs,
    getConfirmMessage,
    getCurrentDate,
    getTabIds,
    chosenTabIsPresent
} from '../../../../__data__/feature/reservation/reservation.selectors';
import {
    confirmReservation,
    clearChosenDates
} from '../../../../__data__/feature/reservation/reservation.slice';
import Modal from '../modal/modal';
import Toaster from '../toaster/toaster';

const Reservation = () => {
    const [reservationPost, { isError, isSuccess }] = usePostTabsMutation();
    const [modalOpen, setModalOpen] = useState(false);
    const [modalMessage, setModalMessage] = useState('');

    const [toasterOpen, setToasterOpen] = useState(false);
    const [toasterMessage, setToasterMessage] = useState('');

    const currentDate = useSelector(getCurrentDate);
    const tabIds = useSelector(getTabIds);
    const isChosen = useSelector(chosenTabIsPresent);
    const postTabs = useSelector(getPostTabs);
    const message = useSelector(getConfirmMessage);
    const dispatch = useDispatch();

    const handleReserve = () => {
        if (!isChosen) {
            setToasterOpen(true);
            setToasterMessage('Choose a table');
            return;
        }
        setModalOpen(true);
        setModalMessage(`Confirm Reservation: \n\n ${message}`);
    };
    const handleModalOk = () => {
        reservationPost(postTabs);
        setModalOpen(false);
    };

    useEffect(() => {
        if (isError) {
            setToasterOpen(true);
            setToasterMessage('Reservation is not completed :(');
        }
    }, [isError]);
    useEffect(() => {
        if (isSuccess) {
            dispatch(confirmReservation());
            dispatch(clearChosenDates());
            setToasterOpen(true);
            setToasterMessage('Reservation completed successfully!');
        }
    }, [isSuccess]);

    return (
        <StyledReservationContainer>
            <ChooseTable date={new Date(currentDate)} />
            <StyledDiv>
                <div>
                    <Hall tabs={tabIds.slice(0, 6)} />
                    <Hall tabs={tabIds.slice(6, 12)} />
                </div>
                <div>
                    <Hall tabs={tabIds.slice(12, 18)} />
                    <Legend />
                </div>
                <div>
                    <Calendar />
                    <StyledReserve
                        onClick={handleReserve}
                        data-testid={'reservation'}
                    >
                        Reserve
                    </StyledReserve>
                </div>
            </StyledDiv>
            <Modal
                isOpen={modalOpen}
                title={modalMessage}
                handleClose={() => setModalOpen(false)}
                handleOk={handleModalOk}
            />
            <Toaster
                isOpen={toasterOpen}
                message={toasterMessage}
                handleClose={() => setToasterOpen(false)}
            />
        </StyledReservationContainer>
    );
};
export default Reservation;
