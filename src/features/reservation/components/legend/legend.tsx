import React from 'react';
import {
    LegendBlock,
    LegendRow,
    LegendSvg,
    StyledSquare
} from './legend.style';

const Legend = () => {
    return (
        <LegendBlock>
            <LegendRow key={1}>
                <LegendSvg>
                    <StyledSquare state={1} />
                </LegendSvg>
                <div>&nbsp;free</div>
            </LegendRow>
            <LegendRow key={2}>
                <LegendSvg>
                    <StyledSquare state={0} />
                </LegendSvg>
                <div>&nbsp;reserved</div>
            </LegendRow>
            <LegendRow key={3}>
                <LegendSvg>
                    <StyledSquare state={2} />
                </LegendSvg>
                <div>&nbsp;in progress</div>
            </LegendRow>
        </LegendBlock>
    );
};
export default Legend;
