import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import DatePicker from 'react-datepicker';
import { StyledCalendar } from './calendar.style';
import { useDispatch, useSelector } from 'react-redux';
import { setCurrentDate } from '../../../../__data__/feature/reservation/reservation.slice';
import { getTimeStamp } from '../../../../data/reservation/tabtype';
import { getCurrentDate } from '../../../../__data__/feature/reservation/reservation.selectors';

const Calendar = () => {
    const dispatch = useDispatch();
    const handleCalendar = (date: Date) => {
        dispatch(setCurrentDate(getTimeStamp(date)));
    };
    const currentDate = useSelector(getCurrentDate);

    return (
        <StyledCalendar>
            <DatePicker
                onSelect={handleCalendar}
                onChange={handleCalendar}
                minDate={new Date()}
                selected={new Date(currentDate)}
                shouldCloseOnSelect={false}
                dateFormat="yyyy-M-d"
            />
        </StyledCalendar>
    );
};
export default Calendar;
