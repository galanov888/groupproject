import { notification } from 'antd';

const showNotification = (type, details) => {
    notification[type]({
        message: details
    });
};
export default showNotification;
