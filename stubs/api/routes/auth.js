// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const authRouter = require('express').Router();
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const bcrypt = require('bcrypt');
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const checkRequiredMidleware = require('../midlewares/checkRequired.midleware');
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const jwt = require('jsonwebtoken');
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const authGuard = require('../midlewares/user.midleware');
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const { getResponse } = require('./dish');

const users = [];
// const saltRounds = Number(getAllFeatures()['saltRounds']);

authRouter.get('/users', authGuard, (req, res) => {
    console.log(req.user);
    res.status(200).send(users);
});

authRouter.post(
    '/register',
    checkRequiredMidleware(['email', 'password']),
    async (req, res) => {
        const { email, password } = req.body;
        // eslint-disable-next-line no-undef
        const hash = await bcrypt.hash(password, Number(process.env.SECRET));
        console.log(hash);
        users.push({ email, hash });
        res.send(getResponse(null, { email, hash }));
    }
);

authRouter.post(
    '/login',
    checkRequiredMidleware(['email', 'password']),
    async (req, res) => {
        const { email, password } = req.body;
        const user = users.find((user) => user.email === email);
        if (user) {
            const passwordCorrect = await bcrypt.compare(password, user.hash);
            if (passwordCorrect) {
                // eslint-disable-next-line no-undef
                const token = jwt.sign({ email }, process.env.SECRET);
                res.send({ email, token });
                return;
            }
        }
        res.status(400).send('Incorrect email or password');
    }
);

// eslint-disable-next-line no-undef
module.exports = authRouter;
