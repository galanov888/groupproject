const theme = {
    font: {
        main: '"JetBrainsMono-Regular", sans-serif',
        title: '"Iceland-Regular", Areal,  sans-serif'
    },
    fontSize: {
        header: '45px',
        xl: '45px',
        l: '36px',
        m: '22px',
        s: '20px',
        xs: '16px'
    },
    spacing: {
        xxl: '60px',
        xl: '45px',
        l: '36px',
        m: '25px',
        s: '20px',
        xs: '10px'
    }
};

const NightColor = {
    color: {
        header: 'rgba(255,116,0,75%)',
        headerHover: '#ff7400',
        title: '#d7d7d7',
        main: '#d7d7d7',
        decor1: 'rgba(255, 116, 0, 0.75)',
        decor1hover: '#ff7400',
        decor2: '#00a438',
        decor3: 'rgb(249,194,107)',
        decor4: 'darkgrey',
        background: '#252525',
        greenPrice: 'rgb(0,164,56)',
        debug: '#252525',
        purple: '#C080B0'
    }
};

const DayColor = {
    color: {
        header: 'rgba(60,120,60,1)',
        headerHover: 'rgb(149,142,100)',
        title: 'rgb(255,247,230)',
        main: 'rgb(91,40,2)',
        decor1: 'rgba(60,120,60,1)',
        decor1hover: 'rgba(60,120,60,1)',
        decor2: '#ff5900',
        decor3: 'rgb(134,15,150)',
        decor4: 'darkgrey',
        background: 'rgb(255,247,230)',
        debug: 'rgb(255,247,230)',
        greenPrice: 'rgb(0,164,56)',
        purple: '#C080B0'
    }
};

export const Day = {
    id: 'Day',
    color: DayColor.color,
    font: theme.font,
    fontSize: theme.fontSize,
    spacing: theme.spacing
};
export const Night = {
    id: 'Night',
    color: NightColor.color,
    font: theme.font,
    fontSize: theme.fontSize,
    spacing: theme.spacing
};
