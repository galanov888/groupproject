import React from 'react';

type ImageProps = {
    path: string;
    alt: string;
};

const Image: React.FC<ImageProps> = ({ path, alt }) => {
    return (
        <div>
            <img src={path} alt={alt} />
        </div>
    );
};

export default Image;
