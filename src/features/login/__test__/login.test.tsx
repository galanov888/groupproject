/* eslint-disable @typescript-eslint/no-empty-function */
import React from 'react';
import 'whatwg-fetch';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import {
    describe,
    it,
    expect,
    beforeAll,
    afterEach,
    afterAll
} from '@jest/globals';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import Login from '../login';
import { store } from '../../../__data__/store';

import { ThemeContextProvider } from '../../../context/themeContext';

const server = setupServer(
    rest.post('/auth/login', async (req, res, ctx) => {
        const { email, password } = await req.json();
        if (email !== 'email@test.ru' || password !== '12345') {
            return res(
                ctx.status(400),
                ctx.body('incorrect email or password')
            );
        }
        return res(
            ctx.json({
                email: 'email',
                token: 'token'
            })
        );
    })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('Login', () => {
    it('Snapshot', () => {
        const { container } = render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <BrowserRouter>
                        <Login handleClose={() => {}} />
                    </BrowserRouter>
                </Provider>
            </ThemeContextProvider>
        );
        expect(container).toMatchSnapshot();
    });

    it('SignIn test ', async () => {
        const { container } = render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <BrowserRouter>
                        <Login handleClose={() => {}} />
                    </BrowserRouter>
                </Provider>
            </ThemeContextProvider>
        );
        fireEvent.change(screen.getByRole('textbox', { name: /email/i }), {
            target: { value: 'email@test.ru' }
        });
        fireEvent.change(screen.getByTestId('password'), {
            target: { value: '1234' }
        });
        fireEvent.click(screen.getByRole('button', { name: /submit/i }));
        await waitFor(() => {
            expect(
                screen.getByText(/`incorrect email or password`/i)
            ).toBeTruthy();
        });
        fireEvent.change(screen.getByTestId('password'), {
            target: { value: '12345' }
        });
        fireEvent.click(screen.getByRole('button', { name: /submit/i }));
        await waitFor(() => {
            expect(localStorage.getItem('token')).toBe('token');
        });
    });
});
