import React from 'react';
import { DishType } from '../../data/dish/dishtypes';
import {
    StyledCounterBlock,
    StyledCounterButton,
    StyledQuantity
} from './counter.style';
import { getCountItem } from '../../__data__/basket/basket.selectors';

import { basketActions } from '../../__data__/basket/basket.slice';
import { useDispatch } from 'react-redux';
import { useAppSelector } from '../../__data__/store.utils';

type CounterProps = {
    dish: DishType;
};

const Counter: React.FC<CounterProps> = ({ dish }) => {
    const currentCount = useAppSelector((state) => getCountItem(state, dish));

    const dispatch = useDispatch();
    const handleDecrement = () => {
        dispatch(basketActions.deleteItem(dish));
    };

    const handleIncrement = () => {
        dispatch(basketActions.addItem(dish));
    };

    let counterBlock;
    if (currentCount) {
        counterBlock = (
            <StyledCounterBlock>
                <StyledCounterButton
                    onClick={handleDecrement}
                    data-testid={'counter-minus'}
                >
                    -
                </StyledCounterButton>
                <StyledQuantity data-testid={'counter-count'}>
                    {currentCount}
                </StyledQuantity>
                <StyledCounterButton
                    onClick={handleIncrement}
                    data-testid={'counter-plus'}
                >
                    +
                </StyledCounterButton>
            </StyledCounterBlock>
        );
    } else {
        counterBlock = (
            <StyledCounterBlock>
                <StyledCounterButton
                    onClick={handleIncrement}
                    data-testid={'counter-add'}
                >
                    &nbsp;add&nbsp;
                </StyledCounterButton>
            </StyledCounterBlock>
        );
    }

    return <div>{counterBlock}</div>;
};

export default Counter;
