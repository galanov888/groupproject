import { getConfigValue } from '@ijl/cli';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { DishType } from '../../data/dishes';
import MenuChoice from '../../features/menu-choice';
import LoadingPage from '../load';

const Menu: React.FC = () => {
    const [dishData, setdishData] = useState<DishType[]>(null);
    const [errorData, setErrorData] = useState<DishType[]>(null);
    const apiUrl = getConfigValue('restaurant.apiUrl');
    useEffect(() => {
        axios
            .get(`${apiUrl}/dish`)
            .then((res) => res.data)
            .then((data) => {
                setdishData(data.data);
            })
            .catch((error) => {
                setErrorData(error.message);
            });
    }, []);
    return (
        <div>
            {dishData ? (
                <MenuChoice dishData={dishData} />
            ) : errorData ? (
                <div>{errorData}</div>
            ) : (
                <LoadingPage></LoadingPage>
            )}
        </div>
    );
};

export default Menu;
