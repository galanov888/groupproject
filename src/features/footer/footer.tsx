import React from 'react';
import { StyledFooter } from './footer.style';

const footerText =
    'Team1: Nikita Galanov, Sergey Kashenkov, Natalia Chesnokovskaya';

const Footer = () => {
    return <StyledFooter>{footerText}</StyledFooter>;
};

export default Footer;
