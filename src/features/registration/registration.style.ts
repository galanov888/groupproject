import styled from '@emotion/styled';
import { Button, Form } from 'antd';
export const StyledButton = styled(Button)`
    background-color: ${(props) => props.theme.color.decor4};
    border: 2px solid ${(props) => props.theme.color.decor1};
    font-weight: 700;
`;

export const StyledForm = styled(Form)`
    .ant-form-item {
        margin-bottom: 20px;
    }
    .ant-form-item-required {
        color: red;
    }
    .ant-col-offset-12 {
        margin-inline-start: 0%;
    }
    .ant-form-item .ant-form-item-label > label {
        color: ${(props) => props.theme.color.decor1};
    }
`;
