import styled from '@emotion/styled';

export const Menu = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
`;

export const MenuTitle = styled.div`
    font-family: ${(props) => props.theme.font.main};
    color: ${(props) => props.theme.color.main};
    font-size: ${(props) => props.theme.fontSize.header};
    padding-bottom: ${(props) => props.theme.spacing.m};
`;
