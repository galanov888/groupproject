import React from 'react';
import { StyledTitle, StyledDate } from './choosetable.style';

const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];
const days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
];

type DateProps = {
    date: Date;
};
const ChooseTable: React.FC<DateProps> = ({ date }) => {
    return (
        <div>
            <StyledTitle>Choose date and table</StyledTitle>
            <StyledDate>
                {months[date.getMonth()]} {date.getDate()} {days[date.getDay()]}
            </StyledDate>
        </div>
    );
};

export default ChooseTable;
