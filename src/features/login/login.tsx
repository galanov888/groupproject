import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { generatePath, Link } from 'react-router-dom';
import { getConfigValue, getNavigationsValue } from '@ijl/cli';
import {
    Box,
    PopupBox,
    StyledCloseIcon,
    StyledError,
    StyledHeader,
    StyledInput,
    StyledLabel
} from './login.style';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { signIn } from '../../__data__/feature/user/user.slice';

type LoginProps = {
    handleClose: () => void;
};

const apiUrl = getConfigValue('restaurant.apiUrl');

const LoginForm: React.FC<LoginProps> = ({ handleClose }) => {
    const dispatch = useDispatch();

    const [error, setError] = useState(null);

    const [formValue, setformValue] = useState({
        email: '',
        password: ''
    });

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios({
                method: 'post',
                url: `${apiUrl}/auth/login`,
                data: {
                    email: formValue.email,
                    password: formValue.password
                }
            });
            dispatch(signIn(response.data['email']));
            onClickClose();
            localStorage.setItem('token', response.data['token']);
        } catch (ex) {
            setError(ex.response.data);
        }
    };

    const onClickClose = () => {
        setError(null);
        handleClose();
    };

    const handleChange = (event) => {
        setformValue({
            ...formValue,
            [event.target.name]: event.target.value
        });
    };

    return ReactDOM.createPortal(
        <PopupBox>
            <Box>
                <StyledCloseIcon onClick={() => onClickClose()}>
                    x
                </StyledCloseIcon>
                <form onSubmit={handleSubmit}>
                    <StyledHeader>Sign in</StyledHeader>
                    <StyledLabel>Email</StyledLabel>
                    <StyledInput
                        aria-label={'email'}
                        type="text"
                        name="email"
                        value={formValue.email}
                        onChange={handleChange}
                    />
                    <StyledLabel>Password</StyledLabel>
                    <StyledInput
                        aria-label={'password'}
                        type="password"
                        name="password"
                        value={formValue.password}
                        onChange={handleChange}
                        data-testid={'password'}
                    />
                    <StyledInput
                        aria-label={'Submit'}
                        type="submit"
                        name="LOGIN"
                    />
                    {error && <StyledError>`{error}`</StyledError>}
                    <StyledLabel>OR</StyledLabel>
                    <Link
                        to={generatePath(
                            getNavigationsValue('restaurant.registration')
                        )}
                        style={{ textDecoration: 'none' }}
                        replace
                    >
                        <StyledHeader>register now!</StyledHeader>
                    </Link>
                </form>
            </Box>
        </PopupBox>,
        document.body
    );
};

export default LoginForm;
