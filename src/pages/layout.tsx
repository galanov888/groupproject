import React from 'react';
import Header from '../features/header/header';
import Footer from '../features/footer/footer';
import { Outlet } from 'react-router-dom';
import {
    GlobalStyles,
    StyledContainer,
    StyledContent,
    StyledMain
} from './layout.style';
import { Global } from '@emotion/react';

const Layout = () => {
    return (
        <StyledContainer>
            <Global styles={GlobalStyles} />
            <StyledContent>
                <Header />
                <StyledMain>
                    <Outlet />
                </StyledMain>
            </StyledContent>
            <Footer />
        </StyledContainer>
    );
};

export default Layout;
