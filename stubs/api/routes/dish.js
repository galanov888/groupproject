// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const waitMidleware = require('../midlewares/wait.middleware');
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const dishRouter = require('express').Router();
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const dishData = require('../data/dishdata.json');

const getResponse = (error, data, success = true) => {
    if (error) {
        return {
            success: false,
            error
        };
    }

    return {
        success,
        data
    };
};

dishRouter.get('/', waitMidleware, (req, res) => {
    res.status(200).send(getResponse(null, dishData));
});

dishRouter.get('/:dishId', (req, res) => {
    const dish = dishData.find((dish) => dish.id === req.params.dishId);
    if (dish) {
        res.status(200).send(getResponse(null, dish));
        return;
    }
    res.status(403).send('dishId not found');
});

dishRouter.get('/alsoLike/:dishId', (req, res) => {
    const dish = dishData.find((el) => el.id === req.params.dishId);
    if (!dish) {
        res.status(403).send('dishId not found');
        return;
    }
    const categoryArr = dishData.filter(
        (el) => el.category === dish.category && el.id !== req.params.dishId
    );
    if (!categoryArr.length) {
        res.status(403).send('dishId not found');
    }
    res.status(200).send(getResponse(null, categoryArr));
});

// eslint-disable-next-line no-undef
module.exports = dishRouter;
