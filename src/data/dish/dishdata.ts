import { DishType, DishCategories } from './dishtypes';

export const DishData: DishType[] = [
    {
        id: 's1',
        category: DishCategories.Salads,
        title: 'Qwerty salad',
        image: 'qwertysalad.jpg',
        price: 244,
        description:
            'Fragrant crispy keys dressed with coffee, beer or coca-cola'
    },
    {
        id: 's2',
        category: DishCategories.Salads,
        title: 'Fibonacci salad',
        image: 'fibonaccisalad.jpg',
        price: 199,
        description:
            'Refined taste of yesterday salad, complimented by salad from two days ago'
    },
    {
        id: 's3',
        category: DishCategories.Salads,
        title: 'RNG salad',
        image: 'rngsalad.jpg',
        price: 317,
        description:
            'Exquisite original taste of fully natural, randomly generated numbers'
    },
    {
        id: 's4',
        category: DishCategories.Salads,
        title: 'Merge request',
        image: 'mergerequestsalad.jpg',
        price: 241,
        description: 'Non-merged salad, ideal for merge conflicts intolerance'
    },
    {
        id: 'cf1',
        category: DishCategories.CFood,
        title: 'SalmON',
        image: 'salmon.jpg',
        price: 512,
        description: 'Salmon with user-friendly interface'
    },
    {
        id: 'cf2',
        category: DishCategories.CFood,
        title: 'SalmOFF',
        image: 'salmoff1.jpg',
        price: 4,
        description:
            'The pink Salmon skeleton is perfect for research and debugging'
    },
    {
        id: 'cf3',
        category: DishCategories.CFood,
        title: 'Develobster',
        image: 'develobster.jpg',
        price: 1024,
        description: 'High skilled senior frontend develobster'
    },
    {
        id: 'cf4',
        category: DishCategories.CFood,
        title: '8-bit Octopus',
        image: 'octopus1.jpg',
        price: 500,
        description: '8-bit Octopus summary'
    },
    {
        id: 'sp1',
        category: DishCategories.Soups,
        title: 'Hooks & props',
        image: 'hookspropssoup.jpg',
        price: 411,
        description:
            'A rich fragrant soup with a large set of properties and states that is easy to hook'
    },
    {
        id: 'sp2',
        category: DishCategories.Soups,
        title: 'Float points',
        image: 'floatpointsoup.jpg',
        price: 433,
        description:
            'A light soup, the original taste and unique aroma of which is provided by floating points'
    },
    {
        id: 'sp3',
        category: DishCategories.Soups,
        title: 'BorscHTML',
        image: 'borschtml.jpg',
        price: 422,
        description:
            'Marked up by sour cream soup based on thoroughly tested bit-root'
    },
    {
        id: 'sp4',
        category: DishCategories.Soups,
        title: 'Try-catch',
        image: 'trycatch3.jpg',
        price: 455,
        description:
            'Soup for real gourmets familiar with advanced try-catch technologies'
    },
    {
        id: 'd1',
        category: DishCategories.Desserts,
        title: 'Fractal',
        image: 'fractal3.jpg',
        price: 378,
        description:
            'Self-similar fractal produced by recursion from dark chocolate and cream'
    },
    {
        id: 'd2',
        category: DishCategories.Desserts,
        title: 'Debugged apple',
        image: 'apple.jpg',
        price: 85,
        description:
            'Eco-friendly apple debugged and tested in the most thorough way'
    },
    {
        id: 'd3',
        category: DishCategories.Desserts,
        title: 'Cookies',
        image: 'cookies2.jpg',
        price: 111,
        description: 'We also use cookies, but not for this'
    },
    {
        id: 'd4',
        category: DishCategories.Desserts,
        title: 'Syntactic sugar',
        image: 'sugar.jpg',
        price: 100,
        description:
            'Syntactic sugar which make it easier for humans to code. Enjoy it!'
    }
];
export default DishData;
