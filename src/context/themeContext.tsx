import React, { useState } from 'react';
import { Night } from '../theme';
import { Theme, ThemeProvider } from '@emotion/react';

export type themeType = {
    theme: Theme;
    setTheme: (input: Theme) => void;
};

export const MyThemeContext = React.createContext<themeType>(null);

export const ThemeContextProvider: React.FC = (props) => {
    const [theme, setTheme] = useState(Night);
    return (
        <ThemeProvider theme={theme}>
            <MyThemeContext.Provider value={{ theme, setTheme }}>
                {props.children}
            </MyThemeContext.Provider>
        </ThemeProvider>
    );
};
