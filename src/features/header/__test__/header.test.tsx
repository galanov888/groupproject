import * as React from 'react';
import { describe, it, beforeAll, afterEach, afterAll } from '@jest/globals';
import { render, waitFor, screen, fireEvent } from '@testing-library/react';
import { setupServer } from 'msw/node';
import '@testing-library/jest-dom';

import { ThemeContextProvider } from '../../../context/themeContext';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../../__data__/store';

import Header from '../header';

const server = setupServer();
beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('Theme toggle component tests', () => {
    it('Theme toggle button switches theme', async () => {
        render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <BrowserRouter>
                        <Header />
                    </BrowserRouter>
                </Provider>
            </ThemeContextProvider>
        );
        await waitFor(() => screen.getByText('light mode'));
        fireEvent.click(screen.getByText('light mode'));
        await waitFor(() => screen.getByText('dark mode'));
        fireEvent.click(screen.getByText('dark mode'));
        await waitFor(() => screen.getByText('light mode'));
    });
});
