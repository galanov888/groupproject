import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface UserState {
    email: string;
    isAuthorized: boolean;
}

const initialState: UserState = {
    email: '',
    isAuthorized: false
};

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        signIn: (state, action: PayloadAction<string>) => {
            state.email = action.payload;
            state.isAuthorized = true;
        },
        signOut: (state) => {
            state.email = '';
            state.isAuthorized = false;
        }
    }
});
export const { signIn, signOut } = userSlice.actions;

export const userSliceName = userSlice.name;

export default userSlice.reducer;
