import React from 'react';
import { describe, it, expect } from '@jest/globals';
import { render, fireEvent, screen } from '@testing-library/react';

import Counter from '../counter';
import { DishCategories } from '../../../data/dishes';
import { ThemeContextProvider } from '../../../context/themeContext';
import { Provider } from 'react-redux';
import { store } from '../../../__data__/store';

describe('Counter', () => {
    const dish = {
        id: 's1',
        category: DishCategories.Salads,
        title: 'Qwerty salad',
        image: 'qwertysalad.jpg',
        price: 256,
        description:
            'Fragrant crispy keys dressed with coffee, beer or coca-cola'
    };

    it('snapshot', () => {
        const { container } = render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <Counter dish={dish} />
                </Provider>
            </ThemeContextProvider>
        );
        expect(container).toMatchSnapshot();
    });

    it('counter test', () => {
        const { container } = render(
            <ThemeContextProvider>
                <Provider store={store}>
                    <Counter dish={dish} />
                </Provider>
            </ThemeContextProvider>
        );
        let count = screen.queryByTestId('counter-count');
        expect(count).toBeNull();
        fireEvent.click(screen.getByTestId('counter-add'));
        expect(screen.getByTestId('counter-count').textContent).toBe('1');
        expect(container).toMatchSnapshot();
        fireEvent.click(screen.getByTestId('counter-plus'));
        expect(screen.getByTestId('counter-count').textContent).toBe('2');
        fireEvent.click(screen.getByTestId('counter-minus'));
        expect(screen.getByTestId('counter-count').textContent).toBe('1');
        fireEvent.click(screen.getByTestId('counter-minus'));
        count = screen.queryByTestId('counter-count');
        expect(count).toBeNull();
    });
});
