import styled from '@emotion/styled';
import { TabState } from '../../../../data/reservation/tabtype';

const free = (props) => props.theme.color.decor1;
const inprogress = (props) => props.theme.color.decor3;
const reserved = (props) => props.theme.color.decor3;

export const StyledSvg = styled.svg`
    width: 80px;
    height: 80px;
`;

export const StyledPolygon = styled.rect<{ state: TabState }>`
    fill: ${(props) =>
        props.state === TabState.inprogress
            ? inprogress
            : TabState.reserved
            ? reserved
            : free};
    opacity: ${(props) => (props.state === TabState.reserved ? 0.4 : 1)};
    rx: 2px;
    :hover {
        cursor: ${(props) =>
            props.state === TabState.reserved ? 'not-allowed' : 'pointer'};
    }
`;
