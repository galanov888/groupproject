import React from 'react';
import { generatePath, Link } from 'react-router-dom';
import { getNavigationsValue } from '@ijl/cli';
import '../common/dish.style';
import { StyledDishImage } from '../common/dish.style';
import {
    StyledRecommendedItem,
    StyledRecommendedTitle
} from './recommended.style';
import { DishType } from '../../../../data/dish/dishtypes';

type RecommendedDishProps = {
    dish: DishType;
};
const RecommendedDish: React.FC<RecommendedDishProps> = ({ dish }) => {
    const imgPath = `${__webpack_public_path__}/remote-assets/dish/${dish.image}`;
    return (
        <StyledRecommendedItem>
            <StyledRecommendedTitle>{dish.title}</StyledRecommendedTitle>
            <Link
                to={generatePath(getNavigationsValue('restaurant.dish'), {
                    dishId: dish.id
                })}
                replace
                data-testid={`alsolike-${dish.id}`}
            >
                <StyledDishImage small src={imgPath} alt={dish.title} />
            </Link>
            <StyledRecommendedTitle data-price={dish.price}>
                0x{dish.price.toString(16)}₽
            </StyledRecommendedTitle>
        </StyledRecommendedItem>
    );
};

export default RecommendedDish;
