import styled from '@emotion/styled';

export const StyledFooter = styled.footer`
    flex: 0 0 auto;
    font-family: ${(props) => props.theme.font.title};
    font-size: ${(props) => props.theme.fontSize.l};
    padding-left: 10px;
    padding-bottom: 10px;
`;
