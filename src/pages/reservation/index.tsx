import React, { useEffect } from 'react';
import Reservation from '../../features/reservation/components/reservation/reservation';
import { useDispatch } from 'react-redux';
import { addTab } from '../../__data__/feature/reservation/reservation.slice';
import { useGetTabsQuery } from '../../__data__/api';

const ReservationPage: React.FC = () => {
    const dispatch = useDispatch();
    const { data, error, isLoading } = useGetTabsQuery('');

    useEffect(() => {
        if (data) {
            data.forEach((item) => {
                dispatch(addTab(item));
            });
        } else {
            // console.log('dispatch tab error ', error);
        }
    }, [data]);
    let message: string;
    if (error) {
        message = 'Reservation is unavailable now :( ';
    }
    if (isLoading) {
        message = 'Loading reservation data...';
    }
    return <div>{data ? <Reservation /> : `${message}`}</div>;
};
export default ReservationPage;
