import styled from '@emotion/styled';

export const StyledText = styled.p`
    color: ${(props) => props.theme.color.purple};
    font-size: ${(props) => props.theme.fontSize.m};
    margin: 20px;
    text-align: center;
    font-family: ${(props) => props.theme.font.title};
`;

export const StyledTextPrice = styled.p`
    color: ${(props) => props.theme.color.greenPrice};
    font-size: ${(props) => props.theme.fontSize.m};
    margin: 20px;
    text-align: center;
    font-family: ${(props) => props.theme.font.title};
`;

export const StyledDivInLine = styled.div`
    flex-direction: row;
    display: flex;
`;

export const StyledInputButton = styled.button`
    background-color: white;
    font-size: 15px;
    height: 30px;
    margin: 10px;
`;

export const StyledBasketItemBox = styled.div`
    background-color: white;
    margin: 10px;
`;

export const StyledCounterBasket = styled.div`
    margin: 20px;
    text-align: center;
`;
