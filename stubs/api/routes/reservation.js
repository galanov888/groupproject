// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const reservationData = require('../data/tabs.json');
// eslint-disable-next-line @typescript-eslint/no-var-requires, no-undef
const reservationRouter = require('express').Router();

reservationRouter.get('/reservation/', (req, res) => {
    const tabs = reservationData;
    res.status(200).send(tabs);
});

reservationRouter.post('/reservation/post', (req, res) => {
    res.send(req.body);
});

// eslint-disable-next-line no-undef
module.exports = reservationRouter;
