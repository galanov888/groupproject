import styled from '@emotion/styled';
import { NavLink } from 'react-router-dom';

export const StyledHeader = styled.header`
    font-family: ${(props) => props.theme.font.title};
    font-size: ${(props) => props.theme.fontSize.xl};
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
`;

export const StyledNavLink = styled(NavLink)`
    color: ${(props) => props.theme.color.header};
    text-decoration: none;
    :hover {
        color: ${(props) => props.theme.color.headerHover};
        background-color: transparent;
        text-decoration: underline;
    }
`;

export const StyledButton = styled.button`
    background-color: transparent;
    border: 0px;
    cursor: pointer;
`;

export const StyledCartImage = styled.img`
    width: 56px;
    height: 56px;
    opacity: 0.75;
    :hover {
        opacity: 1;
    }
`;
