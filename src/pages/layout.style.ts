import styled from '@emotion/styled';
import { css } from '@emotion/react';

export const StyledContainer = styled.div`
    min-height: 100%;
    background-color: ${(props) => props.theme.color.background};
    font-family: ${(props) => props.theme.font.title};
    color: ${(props) => props.theme.color.main};
    font-size: ${(props) => props.theme.fontSize.l};
    display: flex;
    flex-direction: column;
`;
export const StyledContent = styled.div`
    padding: 25px 50px 25px 50px;
    flex: 1 0 auto;
`;
export const StyledMain = styled.main`
    height: 100%;
`;
//Downloaded from https://www.jetbrains.com/lp/mono/
const jetbrainPath = `${__webpack_public_path__}/remote-assets/fonts/jetbrainmono/JetBrainsMono-Regular.woff2`;
//Downloaded rom https://xfont.pro/download/?font=Iceland
const icelandPath = `${__webpack_public_path__}/remote-assets/fonts/iceland/Iceland-Regular.woff2`;
export const GlobalStyles = css`
    #app {
        height: 100%;
    }
    html,
    body {
        height: 100%;
    }
    @font-face {
        font-family: 'JetBrainsMono-Regular';
        src: url(${jetbrainPath});
    }
    @font-face {
        font-family: 'Iceland-Regular';
        src: url(${icelandPath});
    }
`;
