import styled from '@emotion/styled';

export const StyledMenuCategory = styled.div`
    display: grid;
    flex-wrap: wrap;
`;

export const StyledCategory = styled.div`
    color: ${(props) => props.theme.color.main};
    font-size: ${(props) => props.theme.fontSize.l};
`;

export const StyledStaples = styled.div`
    color: ${(props) => props.theme.color.decor3};
    font-size: ${(props) => props.theme.fontSize.l};
`;
export const StyledItems = styled.div`
    display: grid;
`;
