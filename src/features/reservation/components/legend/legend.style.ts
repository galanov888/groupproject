import styled from '@emotion/styled';
import { TabState } from '../../../../data/reservation/tabtype';

const free = (props) => props.theme.color.decor1;
const inprogress = (props) => props.theme.color.decor3;
const reserved = (props) => props.theme.color.decor3;

export const LegendRow = styled.div`
    font-family: ${(props) => props.theme.font.main};
    font-size: ${(props) => props.theme.fontSize.m};
    color: ${(props) => props.theme.color.main};
    flex-wrap: wrap;
    display: flex;
`;
export const LegendSvg = styled.svg`
    width: 20px;
    height: 20px;
`;
export const LegendBlock = styled.div`
    padding-left: 50px;
    padding-top: 50px;
`;

export const StyledSquare = styled.rect<{ state: TabState }>`
    fill: ${(props) =>
        props.state === TabState.inprogress
            ? inprogress
            : TabState.reserved
            ? reserved
            : free};
    opacity: ${(props) => (props.state === TabState.reserved ? 0.4 : 1)};
    width: 20px;
    height: 20px;
    rx: 2px;
    ry: 2px;
`;
