import React from 'react';
import { describe, it } from '@jest/globals';
import { render, screen, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import { ThemeContextProvider } from '../../../context/themeContext';
import Feedback from '../feedback';

describe('Component Footer', () => {
    it('alive', async () => {
        render(
            <ThemeContextProvider>
                <Feedback />
            </ThemeContextProvider>
        );
        await waitFor(() => screen.getByTestId('feedback'));
    });
});
