import React from 'react';
import { EmptyBasketStyled } from './empty-basket.style';

const EmptyBasket: React.FC = () => {
    return (
        <>
            <EmptyBasketStyled>Empty</EmptyBasketStyled>
        </>
    );
};

export default EmptyBasket;
