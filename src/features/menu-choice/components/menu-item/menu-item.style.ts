import styled from '@emotion/styled';

export const StyledMenuItem = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding-left: ${(props) => props.theme.spacing.m};
`;

export const SpanTitle = styled.div`
    display: flex;
    font-family: ${(props) => props.theme.font.main};
    color: ${(props) => props.theme.color.purple};
    font-size: ${(props) => props.theme.fontSize.m};
`;

export const SpanPrice = styled.div`
    font-family: ${(props) => props.theme.font.main};
    color: ${(props) => props.theme.color.greenPrice};
    font-size: ${(props) => props.theme.fontSize.m};
    padding-right: ${(props) => props.theme.spacing.xs};
`;
export const Decor = styled.span`
    color: ${(props) => props.theme.color.decor1};
`;

export const StyleCounter = styled.div`
    margin-left: auto;
    padding-right: ${(props) => props.theme.spacing.xl};
    text-align: center;
`;
