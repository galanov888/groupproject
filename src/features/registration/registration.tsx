import React from 'react';
import { Form, Input } from 'antd';
import UseRegistration from './useRegistration';
import { generatePath, useNavigate } from 'react-router-dom';
import { getNavigationsValue } from '@ijl/cli';
import showNotification from './notification';
import { StyledButton, StyledForm } from './registration.style';

const RegistrationForm: React.FC = () => {
    const [form] = Form.useForm();
    const navigate = useNavigate();
    const onFinish = () => {
        onSubmit();
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const handleSubmission = (result) => {
        if (result.error) {
            showNotification('error', result.error);
        } else {
            form.resetFields();
            navigate(generatePath(getNavigationsValue('restaurant.home')));
        }
    };

    const onSubmit = async () => {
        let values;

        try {
            values = await form.validateFields(['email', 'password']); // Validate the form fields
        } catch (errorInfo) {
            return;
        }
        const result = await UseRegistration(values); // Submit the form data to the backend
        handleSubmission(result);
    };

    return (
        <div
            style={{
                display: 'block',
                width: 700,
                padding: 30
            }}
        >
            <StyledForm
                name="basic"
                form={form}
                labelCol={{
                    span: 8
                }}
                wrapperCol={{
                    span: 16
                }}
                initialValues={{
                    remember: true
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="Email"
                    name="email"
                    labelCol={{ span: 3, offset: 12 }}
                    rules={[
                        {
                            required: true,
                            type: 'email',
                            message: 'The input is not valid E-mail!'
                        }
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    labelCol={{ span: 3, offset: 12 }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!'
                        }
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    wrapperCol={{
                        offset: 12,
                        span: 16
                    }}
                >
                    <StyledButton type="primary" htmlType="submit">
                        Submit
                    </StyledButton>
                </Form.Item>
            </StyledForm>
        </div>
    );
};

export default RegistrationForm;
